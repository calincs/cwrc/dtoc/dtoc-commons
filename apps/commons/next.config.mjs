import withMDX from '@next/mdx';
import withNextIntl from 'next-intl/plugin';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';

/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: {
    ignoreDuringBuilds: true,
  },
  // experimental: { mdxRs: true },
  swcMinify: true,
  output: 'standalone',
  transpilePackages: ['jotai-devtools'],
};

export default withMDX({
  options: {
    providerImportSource: '@mdx-js/react',
    remarkPlugins: [remarkDirective, remarkDirectiveRehype],
  },
})(withNextIntl('./i18n.ts')(nextConfig));
