import { type CommonsDocument } from '@/types';

export const collection: CommonsDocument[] = [
  {
    source: {
      filename: 'doc1.xml',
      url: 'https://raw.githubusercontent.com/lucaju/docs/master/samples/test/doc1.xml',
      repository: {
        provider: 'github',
        ownerType: 'user',
        owner: 'lucaju',
        repo: 'docs',
        branch: 'master',
        path: 'sample/test',
        filename: 'doc1.xml',
      },
    },
  },
  {
    source: {
      url: 'https://gitlab.com/lucaju/docs/-/raw/master/src/sample/test/doc2.xml?ref_type=heads',
    },
  },
  {
    source: {
      filename: 'doc3.xml',
      content: 'doc3.xml',
    },
  },
];
