import { CommonsDocument, DToCConfig } from '@/types';
import Dexie, { Table } from 'dexie';
import { v4 as uuidv4 } from 'uuid';

export interface CommonsRecent extends CommonsDocument {
  id: NonNullable<CommonsDocument['id']>;
  lastOpenedAt: NonNullable<CommonsDocument['lastOpenedAt']>;
}

export class DexieDB extends Dexie {
  recentDocuments!: Table<CommonsRecent, string>;

  constructor() {
    super('DToC-Commons');
    this.version(1).stores({
      recentDocuments: '&id, source.url', // '&' means 'unique'
    });
  }
}

export const db = new DexieDB();

export const clearCache = async () => {
  await db.recentDocuments
    .clear()
    .catch(() => new Error('Clear `recentDocuments` table: Something went wrong.'));
};

export const deleteDb = async () => {
  return await db.delete().catch(() => new Error('Something went wrong.'));
};

// export const addToRecentDocument = async (resource: DToCRecent) => {
//   const id = uuidv4();
//   const document = { id, lastOpenedAt: new Date(), ...resource };
//   await db.recentDocuments.put(document, id);
// };

// export const updateToRecentDocument = async (
//   id: string,
//   partialDToCRecent: Partial<DToCRecent>,
// ) => {
//   const item = await db.recentDocuments.get({ id });
//   if (!item) return;

//   const doc = {
//     ...item,
//     lastOpenedAt: new Date(),
//     ...partialDToCRecent,
//   };

//   try {
//     await db.recentDocuments.put(doc, item.id);
//   } catch (error) {
//     console.error(error);
//   }
// };
