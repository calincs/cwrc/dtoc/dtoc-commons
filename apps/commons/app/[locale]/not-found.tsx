import { Button, Divider, Sheet, Stack, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import NextLink from 'next/link';
import { Logo } from './_components';
import { primary } from './_components/themeRegistry/colors';

export default function NotFound() {
  const t = useTranslations();
  return (
    <Sheet
      component="main"
      sx={{
        height: '100vh',
        width: '100vw',
        py: { xs: 6, sm: 12 },
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Stack alignItems="center" gap={5} justifyContent="center" sx={{ height: '50vh' }}>
        <Stack>
          <Logo width={220} />
          <Typography
            letterSpacing="8px"
            level="h2"
            textAlign="center"
            textTransform="uppercase"
            sx={{ textDecoration: 'line-through', textDecorationStyle: 'dotted' }}
          >
            racking
          </Typography>
        </Stack>
        <Stack alignItems="center" gap={2}>
          <Stack direction="row" gap={2}>
            <Typography level="h3" textTransform="uppercase">
              404
            </Typography>
            <Divider orientation="vertical" />
            <Typography fontWeight={200} level="h3">
              {t('NotFoundPage.This page could not be found')}
            </Typography>
          </Stack>
          <Typography level="body-sm" textAlign="center">
            {t('NotFoundPage.description')}
          </Typography>
        </Stack>
        <NextLink href="/" passHref>
          <Button variant="outlined">{t('NotFoundPage.Go to Homepage')}</Button>
        </NextLink>
      </Stack>
    </Sheet>
  );
}
