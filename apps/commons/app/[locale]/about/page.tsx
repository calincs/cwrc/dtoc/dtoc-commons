import { Sheet, Stack, Typography } from '@mui/joy';
import { Metadata } from 'next';
import { useLocale } from 'next-intl';
import { Suspense } from 'react';
import { Logo } from '../_components';
import { mdxComponents } from '../_components/about/mdxComponents';
import { ContentMdxRemote } from '../_components/mdx';
import { primary } from '../_components/themeRegistry/colors';

export const metadata: Metadata = {
  title: 'DToC - About',
};

const contentPath = 'http://localhost:3000/content/about';

export default function About() {
  const locale = useLocale();
  return (
    <Sheet
      sx={{
        pt: { xs: 2, sm: 4 },
        pb: { xs: 6, sm: 12 },
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Sheet sx={{ maxWidth: 1000, m: '0 auto', backgroundColor: 'transparent' }}>
        <Stack alignItems="center" gap={10}>
          <Suspense fallback={null}>
            <Logo />
            <Typography level="h1">DToC - Dynamic Table of Contexts</Typography>
          </Suspense>
          <Suspense fallback={<>loading</>}>
            <ContentMdxRemote components={mdxComponents} locale={locale} path={contentPath} />
          </Suspense>
        </Stack>
      </Sheet>
    </Sheet>
  );
}
