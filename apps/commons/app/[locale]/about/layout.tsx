import { authOptions } from '@/app/api/lincs/_auth';
import { getServerSession } from 'next-auth';
import { Footer, NavBar, Profile, TopBar } from '../_components';

export default async function AboutLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  const session = await getServerSession(authOptions);
  return (
    <main style={{ height: '100vh', width: '100vw' }}>
      <TopBar left={<NavBar />} right={session && <Profile session={session} />} />
      {children}
      <Footer />
    </main>
  );
}
