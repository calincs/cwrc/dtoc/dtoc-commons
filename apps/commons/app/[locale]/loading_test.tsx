import CircularProgress from '@mui/joy/CircularProgress';

export default function Loading() {
  // You can add any UI inside Loading, including a Skeleton.
  return <CircularProgress />;
}
