import { createTranslator, NextIntlClientProvider } from 'next-intl';
import { Inter } from 'next/font/google';
import { notFound } from 'next/navigation';
import '../globals.css';
import { ThemeRegistry } from './_components';
import { NextAuthProvider } from './NextAuthProvider';
import { JotaiProvider } from './jotaiProvider';

const inter = Inter({ subsets: ['latin'] });

interface Props {
  children: React.ReactNode;
  params: { locale: string };
}

async function getMessages(locale: string) {
  try {
    return (await import(`../../locales/${locale}.json`)).default;
  } catch (error) {
    notFound();
  }
}

export async function generateStaticParams() {
  return ['en', 'fr'].map((locale) => ({ locale }));
}

export async function generateMetadata({ params: { locale } }: Props) {
  const messages = await getMessages(locale);

  // You can use the core (non-React) APIs when you have to use next-intl
  // outside of components. Potentially this will be simplified in the future
  // (see https://next-intl-docs.vercel.app/docs/next-13/server-components).
  const t = createTranslator({ locale, messages });

  return {
    title: t('LocalePageMeta.title'),
  };
}

export default async function LocaleLayout({ children, params: { locale } }: Props) {
  const messages = await getMessages(locale);

  return (
    <html lang={locale}>
      <body className={inter.className}>
        <JotaiProvider>
          <NextAuthProvider>
            <NextIntlClientProvider locale={locale} messages={messages} timeZone="America/Montreal">
              <ThemeRegistry>{children}</ThemeRegistry>
            </NextIntlClientProvider>
          </NextAuthProvider>
        </JotaiProvider>
      </body>
    </html>
  );
}
