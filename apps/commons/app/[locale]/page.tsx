import { Sheet, Stack } from '@mui/joy';
import { getServerSession } from 'next-auth';
import { authOptions } from '../api/lincs/_auth';
import { AboutContent, Footer, NavBar, Profile, TopBar } from './_components';
import { Header, Main } from './_homepage';
import { primary } from './_components/themeRegistry/colors';



export default async function Homepage() {
  const session = await getServerSession(authOptions);
  return (
    <main style={{
      height: '100vh',
      width: '100vw',
      backgroundColor: primary['100']
    }}
    >
      <TopBar
        left={<NavBar />}
        right={session && <Profile session={session} />}
      />
      <Sheet
        id="bg"
        sx={{
          height: '100vh',
          minHeight: 900,
          mx: 'auto',
          p: 8,
          background: `radial-gradient(circle at 50% 100%, ${primary['200']} 50%, ${primary['50']} 100%);`,
          transition: 'all 0.4s',
        }}
      >
        <Stack alignItems="center" gap={10}>
          <Header isSignedIn={!!session} />
          <Main />
        </Stack>
      </Sheet>
      {!session && <AboutContent />}
      <Footer />
    </main>
  );
}
