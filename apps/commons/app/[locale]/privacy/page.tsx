import { Sheet } from '@mui/joy';
import { Metadata } from 'next';
import { useLocale } from 'next-intl';
import { Suspense } from 'react';
import { ContentMdxRemote } from '../_components/mdx';
import { mdxComponents } from './components/mdxComponents';
import { primary } from '../_components/themeRegistry/colors';

export const metadata: Metadata = {
  title: 'DToC - Privacy Policy',
};

const contentPath = 'http://localhost:3000/content/privacy-policy';

export default function Privacy() {
  const locale = useLocale();
  return (
    <Sheet
      sx={{
        py: { xs: 6, sm: 12 },
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Sheet sx={{ maxWidth: 1000, m: '0 auto', backgroundColor: 'transparent' }}>
        <Suspense fallback={<>loading</>}>
          <ContentMdxRemote components={mdxComponents} locale={locale} path={contentPath} />
        </Suspense>
      </Sheet>
    </Sheet>
  );
}
