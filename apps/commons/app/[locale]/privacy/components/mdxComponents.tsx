import { Stack, Typography } from '@mui/joy';
import type { MDXComponents } from 'mdx/types';

export const mdxComponents: MDXComponents = {
  wrapper: ({ children }) => (
    <Stack p={{ xs: 2, sm: 0 }} spacing={4}>
      {children}
    </Stack>
  ),
  h1: ({ children }) => (
    <Typography level="h1" textAlign="center">
      {children}
    </Typography>
  ),
  h2: ({ children }) => <Typography level="h2">{children}</Typography>,
  h3: ({ children }) => <Typography level="h3">{children}</Typography>,
  h4: ({ children }) => <Typography level="h4">{children}</Typography>,
  p: ({ children }) => <Typography>{children}</Typography>,
};
