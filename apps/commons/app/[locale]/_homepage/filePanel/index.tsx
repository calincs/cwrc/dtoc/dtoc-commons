'use client';

import { Storage } from '@/app/_components/dialogs';
import { AlertDialog } from '@/app/_components/dialogs/alert';
import { useFile, usePermalink } from '@/app/_hooks';
import { db } from '@/db';
import { alertDialogAtom } from '@/jotai/store';
import type { Resource } from '@cwrc/leafwriter-storage-service';
import { CircularProgress, Modal, Sheet, Stack } from '@mui/joy';
import { useLiveQuery } from 'dexie-react-hooks';
import { motion, type Variants } from 'framer-motion';
import { useAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { resourceAtom } from '@/jotai/store';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { enqueueSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { Main } from './main';
import { SidePanel, type Source } from './sidebar';
import { Topbar, type View } from './topbar';

export const FilePanel = () => {
  const { parseResource } = useFile();
  const { getResourceFromPermalink, resetPermalink } = usePermalink();
  const router = useRouter();
  const { status: sessionStatus } = useSession();
  const resetResource = useResetAtom(resourceAtom);

  const recentDocumentsCount = useLiveQuery(() => db.recentDocuments.count() ?? 0);

  const [alertDialog, setAlerDialog] = useAtom(alertDialogAtom);
  const [loading, setLoading] = useState(false);
  const [selectedView, setSelectedView] = useState<View | undefined>(
    // sessionStatus === 'unauthenticated' ? 'sample' : undefined,
    undefined,
  );
  const [source, setSource] = useState<Source | undefined>(undefined);

  useEffect(() => {
    if (sessionStatus !== 'loading') {
      const permalink = getResourceFromPermalink();
      if (!permalink) return;
      if (!permalink.valid) {
        enqueueSnackbar(permalink.message, { variant: permalink.type });
        return;
      }

      setSource(permalink.resource.url ? 'url' : 'cloud');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sessionStatus]);

  useEffect(() => {
    if (recentDocumentsCount && recentDocumentsCount > 0) {
      setSelectedView('recent');
      return;
    } else {
      setSelectedView('sample');
    }
  }, [recentDocumentsCount]);

  useEffect(() => {
    if (source === 'new') {
      router.push('/view');
    }
  }, [source, router]);

  const variants: Variants = {
    initial: { opacity: 0 },
    default: { opacity: 1 },
  };

  const handleCloseStorage = async (resource?: Resource) => {
    setSource(undefined);
    if (!resource) return;

    setLoading(true);
    const id = await parseResource(resource);
    if (!id) {
      setLoading(false);
      return;
    }

    const params = new URLSearchParams({ document: id });
    router.push(`/view?${params}`);
    // setLoading(false);
  };

  const handleCancel = () => {
    setSource(undefined);
    resetPermalink();
    resetResource();
  };

  return (
    <Sheet
      component={motion.div}
      sx={{
        display: { xs: 'none', sm: 'flex' },
        width: { sm: 600, md: 800, lg: 1000 },
        minHeight: { sm: 320, lg: recentDocumentsCount && recentDocumentsCount > 0 ? 400 : 320 },
        overflow: 'hidden',
        backgroundColor: 'white',
        transition: 'all 0.4s ease-in',
        borderRadius: '8px',
      }}
      variants={variants}
      initial="initial"
      animate={recentDocumentsCount === undefined ? 'initial' : 'default'}
    >
      <SidePanel onSelectSource={(value) => setSource(value)} />
      <Sheet sx={{ p: 1, width: { sm: 360, md: 560, lg: 760 }, backgroundColor: 'transparent' }}>
        <Stack>
          <Topbar onSelectView={(value) => setSelectedView(value)} selectedView={selectedView} />
          <Main selectedView={selectedView} />
        </Stack>
      </Sheet>
      {alertDialog && (
        <Modal onClose={() => setAlerDialog(undefined)} open={!!alertDialog}>
          <AlertDialog {...alertDialog} />
        </Modal>
      )}
      {source && source !== 'new' && (
        <Storage onCancel={handleCancel} onClose={handleCloseStorage} source={source} type="load" />
      )}
      {loading && (
        <Modal
          open={loading}
          sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
        >
          <CircularProgress />
        </Modal>
      )}
    </Sheet>
  );
};
