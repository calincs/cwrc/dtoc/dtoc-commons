import { Box, ListItemDecorator, MenuItem, MenuList, Stack, Tooltip, Typography } from '@mui/joy';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { IconType } from 'react-icons';
import { BsCloud, BsFileEarmarkPlus } from 'react-icons/bs';
import { GoPaste } from 'react-icons/go';
import { MdComputer, MdLanguage } from 'react-icons/md';

export type Source = 'cloud' | 'local' | 'paste' | 'url' | 'new';

export interface ViewOption {
  disabled?: boolean;
  hide?: boolean;
  Icon: IconType;
  label: string;
  tooltipText?: string;
  value: Source;
}

interface Props {
  onSelectSource?: (value: Source | undefined) => void;
}

export const SidePanel = ({ onSelectSource }: Props) => {
  const { data: session, status } = useSession();
  const t = useTranslations();

  const options: ViewOption[] = [
    {
      disabled: !session,
      Icon: BsCloud,
      label: t('FilePanel.From the Cloud'),
      tooltipText: !session
        ? t('FilePanel.You must sign in to open and save documents from the cloud')
        : undefined,
      value: 'cloud',
    },
    { Icon: MdLanguage, label: t('FilePanel.From URL'), value: 'url' },
    { Icon: MdComputer, label: t('FilePanel.From your device'), value: 'local' },
    { Icon: GoPaste, label: t('FilePanel.Paste'), value: 'paste' },
    { Icon: BsFileEarmarkPlus, label: 'New', value: 'new' },
  ];

  return (
    <Stack width={240} gap={3} py={2} px={2}>
      <Typography level="title-lg" ml={6.5} textTransform="uppercase">
        {t('FilePanel.open')}
      </Typography>
      <MenuList sx={{ backgroundColor: 'transparent', gap: 0.25 }} variant="plain">
        {options
          .filter(({ hide }) => hide !== true)
          .map(({ disabled, label, Icon, tooltipText, value }) => (
            <Tooltip
              arrow
              key={value}
              placement="right"
              size="sm"
              title={tooltipText && <Box width={200}>{tooltipText}</Box>}
              variant="soft"
            >
              <span>
                <MenuItem
                  disabled={disabled}
                  onClick={() => onSelectSource?.(value)}
                  sx={{ borderRadius: 4 }}
                >
                  <ListItemDecorator>
                    <Icon />
                  </ListItemDecorator>
                  {label}
                </MenuItem>
              </span>
            </Tooltip>
          ))}
      </MenuList>
    </Stack>
  );
};
