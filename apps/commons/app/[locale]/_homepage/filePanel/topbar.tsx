import { db } from '@/db';
import { Button, Divider, Stack, ToggleButtonGroup } from '@mui/joy';
import { useLiveQuery } from 'dexie-react-hooks';
import { useTranslations } from 'next-intl';
import { IconType } from 'react-icons';
import { HiOutlineDocumentText } from 'react-icons/hi';

export type View = 'recent' | 'sample';

export interface ViewOption {
  disabled?: boolean;
  hide?: boolean;
  Icon: IconType;
  label: string;
  tooltipText?: string;
  value: View;
}

interface Props {
  selectedView?: View;
  onSelectView: (value: View | undefined) => void;
}

export const Topbar = ({ onSelectView, selectedView }: Props) => {
  const recentDocumentsCount = useLiveQuery(() => db.recentDocuments.count() ?? 0);
  const t = useTranslations();

  const options: ViewOption[] = [
    {
      disabled: recentDocumentsCount === 0,
      hide: !recentDocumentsCount,
      Icon: HiOutlineDocumentText,
      label: t('FilePanel.recent'),
      value: 'recent',
    },
    { Icon: HiOutlineDocumentText, label: t('FilePanel.sample'), value: 'sample' },
  ];

  return (
    <Stack gap={1}>
      <ToggleButtonGroup
        onChange={(event, newValue) => {
          if (!newValue) return;
          onSelectView(newValue);
        }}
        spacing={1}
        size="sm"
        sx={{ display: 'flex', justifyContent: 'center' }}
        value={selectedView}
        variant="plain"
      >
        {options
          .filter((option) => !option.hide)
          .map(({ disabled, label, value }) => (
            <Button
              key={value}
              disabled={disabled}
              value={value}
              variant={selectedView === value ? 'solid' : 'plain'}
            >
              {label}
            </Button>
          ))}
      </ToggleButtonGroup>
      <Divider />
    </Stack>
  );
};
