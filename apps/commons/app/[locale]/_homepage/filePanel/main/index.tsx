import { SwrFetcher } from '@/app/_util';
import { CommonsRecent, db } from '@/db';
import { Stack } from '@mui/joy';
import { useLiveQuery } from 'dexie-react-hooks';
import { AnimatePresence } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import useSWR from 'swr';
import { View } from '../topbar';
import { Item } from './item';

interface Props {
  selectedView?: View;
}

export const Main = ({ selectedView }: Props) => {
  const { data: session } = useSession();

  const { data: sample } = useSWR<CommonsRecent[], string>('api/sample', SwrFetcher);

  const recent = useLiveQuery(() =>
    db.recentDocuments.toCollection().reverse().sortBy('lastOpenedAt'),
  );

  const [collection, setCollection] = useState<CommonsRecent[]>([]);
  const [selected, setSelected] = useState<string | null>(null);

  useEffect(() => {
    const _collection = selectedView === 'recent' && recent ? recent : sample ?? [];
    if (!_collection) return;
    setCollection(_collection);
  }, [
    recent,
    sample,
    selectedView
  ]);

  const handleSelection = (id: string) => setSelected(selected !== id ? id : null);

  return (
    <Stack
      direction={selectedView === 'sample' ? 'row' : 'column'}
      alignItems={selectedView === 'sample' ? 'flex-start' : 'stretch'}
      height={{
        sm: session ? 500 : 430,
        md: session ? 500 : 400,
        lg: session ? 500 : 325,
      }}
      overflow="auto"
      py={1}
      gap={1}
    >
      <AnimatePresence>
        {collection.map((document) => (
          <Item
            key={document.id}
            isSample={selectedView === 'sample'}
            {...document}
            selected={selected === document.id}
            setSelected={handleSelection}
          />
        ))}
      </AnimatePresence>
    </Stack>
  );
};
