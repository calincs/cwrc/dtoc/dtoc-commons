'use client';

import { Divider, Stack } from '@mui/joy';
import { motion } from 'framer-motion';
import { useTranslations } from 'next-intl';
import { Props } from '../item';
import { Header } from './header';
import { SourceInfo } from './sourceInfo';

export * from './removeButton';

export const Details = ({ source }: Props) => {
  const t = useTranslations();

  return (
    <Stack
      component={motion.div}
      gap={1}
      initial={{ height: 0 }}
      animate={{ height: 'auto' }}
      exit={{ height: 0 }}
      overflow="hidden"
    >
      <Divider />
      <Stack direction="row" gap={0.5}>
        <Stack>
          <Header source={source}>{t('commons.document')}</Header>
        </Stack>
        <Stack overflow="scroll">
          <SourceInfo source={source} />
        </Stack>
      </Stack>
    </Stack>
  );
};
