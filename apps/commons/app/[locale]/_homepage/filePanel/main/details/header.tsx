'use client';

import { CommonsDocument } from '@/types';
import { Box, Stack, Typography } from '@mui/joy';
import { type PropsWithChildren } from 'react';
import { AiFillGithub, AiFillGitlab } from 'react-icons/ai';
import { MdComputer, MdLanguage } from 'react-icons/md';

interface Props extends PropsWithChildren {
  source: CommonsDocument['source'];
}

export const Header = ({ children, source }: Props) => {
  return (
    <Stack direction="row" alignItems="flex-start" justifyContent="space-between" gap={0.5}>
      <Typography fontWeight={700} level="body-xs" sx={{ textTransform: 'capitalize' }}>
        {children}
      </Typography>
      <Box height={16}>
        {'repository' in source ? (
          <>
            {source.repository.provider === 'github' && <AiFillGithub />}
            {source.repository.provider === 'gitlab' && <AiFillGitlab />}
          </>
        ) : 'url' in source ? (
          <MdLanguage />
        ) : (
          <MdComputer />
        )}
      </Box>
    </Stack>
  );
};
