import { type CommonsDocument } from '@/types';
import { Typography } from '@mui/joy';

interface Props {
  source: CommonsDocument['source'];
}

export const SourceInfo = ({ source }: Props) => {
  return (
    <Typography level="body-xs" pt="1px" sx={{ whiteSpace: 'nowrap' }}>
      {'repository' in source ? (
        <>
          <Typography mr={0.5} variant="soft">
            {source.repository.owner}
          </Typography>
          <Typography mr={0.5} variant="soft">
            {source.repository.repo}
          </Typography>
          {source.repository.path !== '' && `${source.repository.path}/`}
          {source.repository.filename}
        </>
      ) : 'url' in source ? (
        source.url
      ) : (
        'local storage'
      )}
    </Typography>
  );
};
