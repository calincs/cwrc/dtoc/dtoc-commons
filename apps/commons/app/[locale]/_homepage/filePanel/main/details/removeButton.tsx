'use client';

import { db } from '@/db';
import { CommonsDocument } from '@/types';
import { IconButton } from '@mui/joy';
import { BiMinusCircle } from 'react-icons/bi';

export interface Props {
  id: NonNullable<CommonsDocument['id']>;
}

export const RemoveButton = ({ id }: Props) => {
  const handleRemove = async () => await db.recentDocuments.delete(id);

  return (
    <IconButton onClick={handleRemove} size="sm" sx={{ borderRadius: '50%' }}>
      <BiMinusCircle />
    </IconButton>
  );
};
