'use client';

import { CommonsDocument } from '@/types';
import { ListItem, ListItemButton, Stack, Typography } from '@mui/joy';
import { formatDistanceToNow } from 'date-fns';
import { AnimatePresence, motion } from 'framer-motion';
import { useRouter } from 'next/navigation';
import { AiOutlineClockCircle } from 'react-icons/ai';
import { Details, RemoveButton } from './details';
import { CommonsRecent } from '@/db';

export interface Props extends CommonsRecent {
  isSample?: boolean;
  selected: boolean;
  setSelected: (id: string) => void;
}

export const Item = (props: Props) => {
  const router = useRouter();

  const { isSample = false, selected, setSelected, ...commonsDocument } = props;
  const { source, id, lastOpenedAt } = commonsDocument;

  const filename = 'filename' in source ? source.filename : `untitled-${id}`;

  const lastDate =
    lastOpenedAt &&
    formatDistanceToNow(new Date(lastOpenedAt), {
      includeSeconds: true,
      addSuffix: true,
    });

  const handleSelected = () => {
    if (!selected) setSelected(id);
  };

  const handleOpen = async () => {
    if (!selected) setSelected(id);

    const params = new URLSearchParams({ document: id });
    router.push(`/view?${params}`);
  };

  return (
    <ListItem
      color={selected ? 'primary' : 'neutral'}
      component={motion.li}
      sx={{ mx: selected ? 0 : 1, borderRadius: 4, overflow: 'hidden', userSelect: 'none' }}
      variant={selected ? 'outlined' : 'plain'}
      layout="preserve-aspect"
      animate={{ height: 'auto', opacity: 1 }}
      exit={{ height: 0, opacity: 0 }}
    >
      <ListItemButton onClick={handleSelected} onDoubleClick={handleOpen} selected={selected}>
        <Stack sx={{ p: 1 }} width="100%" gap={0.5}>
          <Stack direction="row" gap={2} alignItems="center" justifyContent="space-between">
            <Stack>
              <Typography level="title-sm">{filename}</Typography>
              {lastDate && (
                <Stack direction="row" alignItems="center" gap={0.25}>
                  <AiOutlineClockCircle size={10} />
                  <Typography level="body-xs">{lastDate}</Typography>
                </Stack>
              )}
            </Stack>
            {selected && !isSample && <RemoveButton id={id} />}
          </Stack>
          <AnimatePresence>{selected && !isSample && <Details {...props} />}</AnimatePresence>
        </Stack>
      </ListItemButton>
    </ListItem>
  );
};
