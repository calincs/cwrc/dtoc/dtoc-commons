'use client';

import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';

export const MobileMessage = () => {
  const t = useTranslations();

  return (
    <Typography
      display={{ xs: 'block', sm: 'none' }}
      letterSpacing="0.05rem"
      level="body-sm"
      textAlign="center"
    >
      {t('mobile.largerScreenMessage')}
    </Typography>
  );
};
