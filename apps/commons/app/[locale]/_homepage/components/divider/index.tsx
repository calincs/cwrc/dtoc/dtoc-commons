'use client';

import { Box, useColorScheme } from '@mui/joy';
import classNames from 'classnames';
import styles from './styles.module.css';

const lightFill = '#fbfcfe';
const darkFill = '#0b0d0e';

export const Divider = () => {
  const { mode, systemMode } = useColorScheme();

  const fill =
    mode === 'dark' || (mode === 'system' && systemMode === 'dark') ? darkFill : lightFill;

  return (
    <>
      <Box
        id="divider"
        className={classNames(styles.shapeDivider, styles.lightTheme, styles.lightDivider)}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 1200 360"
          preserveAspectRatio="none"
          fill={!mode ? lightFill : fill}
        >
          <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" />
        </svg>
      </Box>
      <Box
        id="divider"
        className={classNames(styles.shapeDivider, styles.darkTheme, styles.darkDivider)}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 1200 360"
          preserveAspectRatio="none"
          fill={!mode ? darkFill : fill}
        >
          <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" />
        </svg>
      </Box>
    </>
  );
};
