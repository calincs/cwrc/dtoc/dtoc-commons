'use client';

import 'client-only';
import { DevTools } from 'jotai-devtools';
import { Divider } from './components';
import { FilePanel } from './filePanel';

export const Main = () => {
  return (
    <>
      <FilePanel />
      {/* <DevTools /> */}
      {/* <Divider /> */}
    </>
  );
};
