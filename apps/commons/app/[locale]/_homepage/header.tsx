'use client';

import { Button, Stack, Typography } from '@mui/joy';
import 'client-only';
import { signIn } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { Suspense } from 'react';
import { Logo } from '../_components';
import { MobileMessage } from './components';

export const Header = ({ isSignedIn }: { isSignedIn: boolean }) => {
  const t = useTranslations();

  const handleClick = () => {
    void signIn('keycloak', undefined, { prompt: 'login' });
  };

  return (
    <Stack direction="row" justifyContent="center" alignItems="center" gap={10}>
      <Stack alignItems="center" gap={2}>
        <Suspense fallback={null}>
          <Stack direction="row">
            <Logo />
            <Stack
              direction="column"
              alignItems="start"
              justifyContent="center"
              spacing={1}
              sx={{ ml: '1em' }}
            >
              <Typography level="h1" letterSpacing={1}>
                DToC
              </Typography>
              <Typography level="h2">Dynamic Table of Contexts</Typography>
            </Stack>
          </Stack>
        </Suspense>
        {!isSignedIn && (
          <Button
            onClick={handleClick}
            size="lg"
            sx={{ display: { xs: 'none', sm: 'inline-flex' } }}
          >
            {t('common.signIn')}
          </Button>
        )}
        <MobileMessage />
      </Stack>
      {!isSignedIn && <></>}
    </Stack>
  );
};
