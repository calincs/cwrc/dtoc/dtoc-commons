import { Link as LinkIntl } from '@/app/navigation';
import { Button, Stack } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { UrlObject } from 'url';
import { Logo } from '../';

interface MenuOption {
  disabled?: boolean;
  hide?: boolean;
  href: string | UrlObject;
  label: string;
  value: string;
}

interface Props {
  activeOption?: string;
  hideLogo?: boolean;
  options?: Pick<MenuOption, 'value' | 'disabled' | 'hide'>[];
}

export const NavBar = ({ activeOption, hideLogo = false, options }: Props) => {
  const t = useTranslations();

  const menuOption: MenuOption[] = [{ href: '/about', label: t('common.about'), value: 'about' }];

  const filteredOptions = menuOption.filter((menuOption) => {
    const hideOption = options?.some((option) => {
      return option.value === menuOption.value && option.hide === true;
    });
    if (!hideOption) return menuOption;
    return hideOption !== true;
  });

  return (
    <Stack direction="row" alignItems="center" gap={2}>
      {!hideLogo && (
        <LinkIntl href="/" passHref>
          <Button
            startDecorator={<Logo height={26} invertColor />}
            sx={{ backgroundColor: 'transparent' }}
            size="sm"
          >
            DToC
          </Button>
        </LinkIntl>
      )}
      <Stack direction="row" alignItems="center" gap={2}>
        {filteredOptions.map(({ disabled, href, label, value }) => (
          <LinkIntl key={value} href={href} passHref>
            <Button
              size="sm"
              disabled={disabled}
              sx={{ textTransform: 'capitalize', backgroundColor: 'transparent' }}
              variant={value === activeOption ? 'outlined' : 'solid'}
            >
              {label}
            </Button>
          </LinkIntl>
        ))}
      </Stack>
    </Stack>
  );
};
