import { extendTheme } from '@mui/joy/styles';
import { primary } from './colors';
import { Roboto_Slab } from 'next/font/google';

export const robotoSlab = Roboto_Slab({
  subsets: ['latin', 'latin-ext']
})

//* https://uxdesign.cc/defining-colors-in-your-design-system-828148e6210a
// * https://colorbox.io/

export const theme = extendTheme({
  colorSchemes: {
    light: {
      palette: {
        primary
      },
    },
  },
  typography: {
    h1: {
      fontFamily: robotoSlab.style.fontFamily,
    },
    h2: {
      fontFamily: robotoSlab.style.fontFamily,
    },
    h3: {
      fontFamily: robotoSlab.style.fontFamily,
    },
    h4: {
      fontFamily: robotoSlab.style.fontFamily,
    }
  }
});