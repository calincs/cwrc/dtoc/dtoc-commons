const purp = ['#fdfcff','#e4e1f2','#ccc7e4','#b6b0d7','#a19aca','#7d75af','#615794','#49417a','#362f5f','#252045','#16122a','#08070f'];
const teal = ['#ffffff','#e4ecf1','#cbdae3','#b4c8d5','#9eb7c7','#7997ab','#5c7a8f','#446073','#304757','#1f303b','#10191f','#010203'];
const blue = ['#ffffff','#e6f3fd','#cee8fa','#b6dbf6','#9ecff2','#75b7e7','#549fd6','#3a87be','#266da0','#18527c','#0e3856','#082133'];

const steps = ['50','100','200','300','400','500','600','700','800','900'];
export const primary: Record<string, string> = {};
steps.forEach((step, i) => {
  primary[step] = blue[i];
});