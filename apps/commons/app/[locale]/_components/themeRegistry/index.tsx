'use client';

import { GlobalStyles } from '@mui/joy';
import CssBaseline from '@mui/joy/CssBaseline';
import { CssVarsProvider as JoyCssVarsProvider, getInitColorSchemeScript } from '@mui/joy/styles';
import {
  THEME_ID as MATERIAL_THEME_ID,
  Experimental_CssVarsProvider as MaterialCssVarsProvider,
  experimental_extendTheme as materialExtendTheme,
} from '@mui/material/styles';
import { SnackbarProvider } from 'notistack';
import * as React from 'react';
import NextAppDirEmotionCacheProvider from './EmotionCache';
import { theme } from './theme';

const materialTheme = materialExtendTheme();

export const ThemeRegistry = ({ children }: { children: React.ReactNode }) => {
  return (
    <NextAppDirEmotionCacheProvider options={{ key: 'joy' }}>
      {/* {getInitColorSchemeScript({ defaultMode: 'light' })} */}
      <MaterialCssVarsProvider theme={{ [MATERIAL_THEME_ID]: materialTheme }}>
        <JoyCssVarsProvider defaultMode="light" theme={theme}>
          <CssBaseline />
          <GlobalStyles
            styles={{
              // The {selector} is the CSS selector to target the icon.
              // We recommend using a class over a tag if possible.
              '{selector}': {
                color: 'var(--Icon-color)',
                margin: 'var(--Icon-margin)',
                fontSize: 'var(--Icon-fontSize, 20px)',
                width: '1em',
                height: '1em',
              },
            }}
          />
          <SnackbarProvider>{children}</SnackbarProvider>
        </JoyCssVarsProvider>
      </MaterialCssVarsProvider>
    </NextAppDirEmotionCacheProvider>
  );
};
