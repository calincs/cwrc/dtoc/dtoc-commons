import { Grid, Sheet } from '@mui/joy';
import { primary } from '../themeRegistry/colors';


interface Props {
  center?: React.ReactNode;
  left?: React.ReactNode;
  right?: React.ReactNode;
}

export const TopBar = ({ center, left, right }: Props) => {
  return (
    <Sheet
      sx={{
        flexShrink: 0,
        display: 'flex',
        justifyContent: 'center',
        background: `linear-gradient(to top, ${primary['900']}, ${primary['800']});`,
      }}
    >
      <Grid
        container
        p={1}
        sx={{ flexGrow: 1 }}
        columns={3}
        justifyContent="space-evenly"
        alignItems="center"
      >
        <Grid xs pl={1}>
          {left && <div>{left}</div>}
        </Grid>
        <Grid xs display="flex" justifyContent="center">
          {center}
        </Grid>
        <Grid xs display="flex" justifyContent="flex-end">
          {right}
        </Grid>
      </Grid>
    </Sheet>
  );
};
