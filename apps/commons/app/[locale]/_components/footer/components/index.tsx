import { Link as LinkIntl } from '@/app/navigation';
import { Grid, Link, Stack, Typography } from '@mui/joy';
import { type PropsWithChildren } from 'react';

interface LinkProps extends PropsWithChildren {
  disabled?: boolean;
  href: string;
}

export * from './bottomBar';

export const Cell = ({ children }: PropsWithChildren) => (
  <Grid xs={6} sm={4} md={2}>
    <Stack alignItems="flex-start">{children}</Stack>
  </Grid>
);

export const Heading = ({ children }: PropsWithChildren) => (
  <Typography level="title-sm" fontWeight={700} pb={1}>
    {children}
  </Typography>
);

export const IntLink = ({ children, href, disabled }: LinkProps) => (
  <LinkIntl href={href} passHref>
    <Link color="neutral" component="span" disabled={disabled} level="body-xs">
      {children}
    </Link>
  </LinkIntl>
);

export const ExtLink = ({ children, disabled, href }: LinkProps) => (
  <Link color="neutral" disabled={disabled} href={href} level="body-xs" target="_blank">
    {children}
  </Link>
);
