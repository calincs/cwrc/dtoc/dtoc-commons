import { Grid, Link } from '@mui/joy';
import { GrCreativeCommons } from 'react-icons/gr';
import { LocaleSwitcher } from '../../localeSwitcher';

export const BottomBar = () => {
  return (
    <Grid container width="100%" sx={{ flexGrow: 1 }} columns={3} justifyContent="space-evenly">
      <Grid xs>
        <Link
          color="neutral"
          level="body-xs"
          flexGrow={1}
          px={1}
          startDecorator={<GrCreativeCommons />}
          href="https://creativecommons.org/"
          target="_blank"
        >
          Creative Commons
        </Link>
      </Grid>
      <Grid xs display="flex" justifyContent="center">
      </Grid>
      <Grid xs display="flex" justifyContent="flex-end">
        {/* <LocaleSwitcher backgroundColor="transparent" /> */}
      </Grid>
    </Grid>
  );
};
