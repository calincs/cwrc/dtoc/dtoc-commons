import { Grid, Sheet, Stack, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { BottomBar, Cell, ExtLink, Heading, IntLink } from './components';
import { primary } from '../themeRegistry/colors';

export const Footer = () => {
  const t = useTranslations();
  return (
    <Sheet
      invertedColors
      variant="solid"
      sx={{
        display: 'flex',
        justifyContent: 'center',
        p: 2,
        background: `linear-gradient(to top, ${primary['800']}, ${primary['900']});`,
      }}
    >
      <Stack maxWidth={1000} width="100%" alignItems="center" gap={2}>
        <Stack alignItems="center" py={2}>
          <Typography level="h4" letterSpacing={1}>
            DToC
          </Typography>
          <Typography level="body-sm">Dynamic Table of Contexts</Typography>
        </Stack>
        <Grid container width="100%" spacing={2} sx={{ flexGrow: 1 }} py={4} pb={7}>
          <Cell>
            <Heading>{t('Footer.resources')}</Heading>
            <ExtLink href="https://www.leaf-vre.org/docs/documentation/leaf-commons/dynamic-table-of-contexts">
              {t('Footer.documentation')}
            </ExtLink>
            <ExtLink href="https://www.leaf-vre.org/docs/training/tutorials/dtoc-tutorial">
              {t('Footer.tutorial')}
            </ExtLink>
            <ExtLink href="https://gitlab.com/calincs/cwrc/dtoc/dtoc-commons">GitLab</ExtLink>
            <ExtLink href="https://github.com/LEAF-VRE/DToC-samples-and-templates">
              {t('Footer.sample_documents')}
            </ExtLink>
          </Cell>
          <Cell>
            <Heading>{t('Footer.legal')}</Heading>
            <IntLink href="/privacy">{t('Footer.privacy_policy')}</IntLink>
            <ExtLink disabled href="">
              {t('Footer.cookie_preferences')}
            </ExtLink>
          </Cell>
          <Cell>
            <Heading>{t('Footer.contact_us')}</Heading>
            <ExtLink href="mailto:leaf@leaf-vre.org">{t('Footer.email us')}</ExtLink>
            <ExtLink href="https://gitlab.com/calincs/cwrc/dtoc/dtoc-commons/-/issues/new">
              {t('Footer.report_bugs')}
            </ExtLink>
          </Cell>
          <Cell>
            <Heading>{t('Footer.more_tools')}</Heading>
            <ExtLink href="https://leaf-writer.leaf-vre.org">LEAF-Writer</ExtLink>
            <Typography fontSize="xs">NERVE</Typography>
          </Cell>
          <Cell>
            <Heading>{t('Footer.ecosystem')}</Heading>
            <ExtLink href="https://cwrc.ca">CWRC</ExtLink>
            <ExtLink href="https://www.leaf-vre.org">LEAF-VRE</ExtLink>
            <ExtLink href="https://lincsproject.ca">LINCS Project</ExtLink>
          </Cell>
          <Cell>
            <Heading>{t('Footer.sponsors')}</Heading>
            <ExtLink href="https://www.innovation.ca">CFI</ExtLink>
            <ExtLink href="https://www.mellon.org">Mellon Foundation</ExtLink>
            <ExtLink href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx">SSHRC</ExtLink>
            <ExtLink href="https://voyant-tools.info">Voyant Consortium</ExtLink>
          </Cell>
        </Grid>
        <BottomBar />
      </Stack>
    </Sheet>
  );
};
