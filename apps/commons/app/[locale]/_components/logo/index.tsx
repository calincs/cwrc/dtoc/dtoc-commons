'use client';

import DtocLogo from '@/public/logo/dtoc_logo.svg';
import DtocLogoDark from '@/public/logo/dtoc_logo_dark.svg';
import { useColorScheme } from '@mui/joy';
import Image, { unstable_getImgProps as getImgProps } from 'next/image';

const DEFAULT_WIDTH = 150;
const DEFAULT_HEIGHT = 150;

export const Logo = ({
  height,
  width,
  invertColor,
}: {
  height?: number;
  width?: number;
  invertColor?: boolean;
}) => {
  const { mode } = useColorScheme();

  if (width && !height) height = width;
  if (height && !width) width = height;

  if (!width && !height) {
    width = DEFAULT_WIDTH;
    height = DEFAULT_HEIGHT;
  }

  const common = {
    alt: 'DToC',
    width,
    height,
  };

  const {
    props: { srcSet: dark },
  } = getImgProps({ ...common, src: invertColor ? DtocLogo : DtocLogoDark });

  const {
    props: { srcSet: light, ...rest },
  } = getImgProps({ ...common, src: invertColor ? DtocLogoDark : DtocLogo });

  return (
    <picture style={{ display: 'inline-flex', alignItems: 'center' }}>
      {mode === 'dark' ? (
        <source srcSet={dark} />
      ) : mode === 'light' ? (
        <source srcSet={light} />
      ) : (
        <>
          <source media="(prefers-color-scheme: dark)" srcSet={dark} />
          <source media="(prefers-color-scheme: light)" srcSet={light} />
        </>
      )}
      <Image alt="DToC" {...rest} />
    </picture>
  );
};
