'use client';

import { db } from '@/db';
import {
  Divider,
  Dropdown,
  IconButton,
  List,
  ListItemDecorator,
  ListItemButton,
  Menu,
  MenuButton,
  MenuItem,
} from '@mui/joy';
import type { Session } from 'next-auth';
import { signOut } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { PiSignOutBold } from 'react-icons/pi';
import { Identity, User } from './components';
import { Avatar } from './components/avatar';

export const Profile = ({ session }: { session: Session }) => {
  const t = useTranslations();

  const handleSignOut = async () => {
    await db.recentDocuments.clear();
    signOut();
  };

  return (
    <Dropdown>
      {session && (
        <MenuButton
          slots={{ root: IconButton }}
          sx={{ ':hover': { backgroundColor: 'transparent' } }}
          variant="plain"
        >
          <Avatar />
        </MenuButton>
      )}
      <Menu placement="bottom-end" size="sm" sx={{ minWidth: 250 }}>
        <User />
        <Divider sx={{ mb: 0.5 }} />
        <List sx={{ gap: 1, py: 1 }}>
          <Identity />
          <ListItemButton
            onClick={() => window.open('https://cwrc.ca/Documentation/public/DITA_Files-Various_Applications/DToC/OverviewDToC.html')}
            >
            Help
          </ListItemButton>
        </List>
        <Divider sx={{ mb: 0.5 }} />
        <MenuItem onClick={handleSignOut} sx={{ mx: 0.5, borderRadius: 'sm' }}>
          <ListItemDecorator>
            <PiSignOutBold />
          </ListItemDecorator>
          {t('common.signOut')}
        </MenuItem>
      </Menu>
    </Dropdown>
  );
};
