'use client';

import { Avatar, Box, IconButton, Link, Stack, Typography } from '@mui/joy';
import { motion, type Variants } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useState } from 'react';
import { RxReload } from 'react-icons/rx';
import { useUser } from '../hooks/useUser';

export const User = () => {
  const { data: session } = useSession();
  const user = session?.user;

  const { manageAccount, refreshAccount } = useUser();

  const [refreshing, setRefreshing] = useState(false);

  const handleManageAccountClick = () => manageAccount();

  const handleManageRefreshClick = async () => {
    setRefreshing(true);
    await refreshAccount();
    setRefreshing(false);
  };

  const variants: Variants = {
    default: { rotate: 0 },
    animate: {
      rotate: 360,
      transition: { ease: 'linear', duration: 1, repeat: Infinity },
    },
  };

  return (
    <Stack direction="row" p={1} gap={1}>
      <Avatar
        alt={user?.name ?? user?.username}
        color="primary"
        size="sm"
        src={user?.image ?? undefined}
      />
      <Stack>
        <Typography>{user?.name}</Typography>
        <Stack direction="row" alignItems="center" gap={1}>
          <Link
            color="neutral"
            level="body-xs"
            onClick={handleManageAccountClick}
            underline="none"
            variant="plain"
          >
            {user?.username}
          </Link>
          <IconButton
            disabled={refreshing}
            onClick={handleManageRefreshClick}
            size="sm"
            sx={{ minWidth: 12, minHeight: 12 }}
          >
            <Box
              display="flex"
              component={motion.div}
              variants={variants}
              initial="default"
              animate={refreshing ? 'animate' : 'default'}
            >
              <RxReload style={{ width: 8, height: 8 }} />
            </Box>
          </IconButton>
        </Stack>
      </Stack>
    </Stack>
  );
};
