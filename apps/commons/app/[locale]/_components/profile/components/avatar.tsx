'use client';

import { Avatar as AvatarJoy, Badge } from '@mui/joy';
import { motion } from 'framer-motion';
import { useSession } from 'next-auth/react';
import { useMemo } from 'react';
import { AiFillGithub, AiFillGitlab } from 'react-icons/ai';
import { SiOrcid } from 'react-icons/si';

export const Avatar = () => {
  const { data: session } = useSession();
  const image = session?.user.image ?? undefined;
  const name = session?.user.name;
  const preferredIdentityProvider = session?.user.preferredIdentityProvider;
  const username = session?.user.username;

  const IdentityIcon = useMemo(() => {
    if (preferredIdentityProvider === 'github') return <AiFillGithub size={18} />;
    if (preferredIdentityProvider === 'gitlab') return <AiFillGitlab size={18} />;
    if (preferredIdentityProvider === 'orcid') return <SiOrcid size={18} />;
    return null;
  }, [preferredIdentityProvider]);

  return (
    <Badge
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      badgeContent={IdentityIcon}
      badgeInset="10%"
      component={motion.div}
      sx={{ '--Badge-paddingX': '0px', '--Badge-ring': '1px' }}
      variant="plain"
      initial={{ scale: 0 }}
      animate={{ scale: 1 }}
    >
      <AvatarJoy alt={name ?? username} color="primary" size="sm" src={image} />
    </Badge>
  );
};
