'use client';

import { ListItem, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import { MdFingerprint } from 'react-icons/md';
import { Selector } from './components';

export const Identity = () => {
  const t = useTranslations();

  const [hover, setHover] = useState(false);

  return (
    <ListItem
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
      sx={{ alignItems: 'center', justifyContent: 'space-between', gap: 1, px: 1.5 }}
    >
      <Typography
        level="body-sm"
        startDecorator={<MdFingerprint style={{ marginRight: 14 }} />}
        sx={{ color: ({ palette }) => (hover ? palette.text.secondary : palette.text.tertiary) }}
      >
        {t('common.identity')}
      </Typography>
      <Selector hover={hover} />
    </ListItem>
  );
};
