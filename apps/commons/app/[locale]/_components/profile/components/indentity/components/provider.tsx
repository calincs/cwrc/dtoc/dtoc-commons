'use client';

import { Option, Typography } from '@mui/joy';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import { useProviders } from '../useProviders';
import { Icon } from './icon';
import { Status } from './status';

interface Props {
  disabled?: boolean;
  linked?: boolean;
  providerId: string;
  selected?: boolean;
}

export const Provider = ({
  disabled = false,
  linked = false,
  providerId,
  selected = false,
}: Props) => {
  const { isProviderLinked, isTokenExpired, linkProvider, refreshToken } = useProviders(providerId);
  const { data: session, update } = useSession();

  const [tokenExpired, setTokenExpired] = useState(false);

  const isLinked = isProviderLinked(providerId);

  useEffect(() => {
    void checkTokens();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session]);

  const checkTokens = async () => {
    const isExpired = await isTokenExpired();
    setTokenExpired(isExpired);
  };

  const handleClick = async () => {
    if (isLinked && tokenExpired) {
      const refreshed = await refreshToken();
      if (!refreshed) return;
    }

    if (!isLinked) {
      const linked = await linkProvider();
      if (!linked) return;
    }

    await update({
      reason: 'changePreferredIdentityProvider',
      preferredIdentityProvider: providerId,
    });
  };

  return (
    <Option
      key={providerId}
      label={providerId}
      value={providerId}
      sx={{
        justifyContent: 'space-between',
        minWidth: 160,
        mx: 0.5,
        borderRadius: 'sm',
        textTransform: 'capitalize',
      }}
      disabled={disabled}
      onClick={handleClick}
    >
      <Typography
        level="body-sm"
        startDecorator={<Icon {...{ providerId }} />}
        sx={{ color: ({ palette }) => palette.text.secondary }}
      >
        {providerId}
      </Typography>
      <Status
        status={selected ? 'selected' : !linked ? 'unlinked' : tokenExpired ? 'expired' : undefined}
      />
    </Option>
  );
};
