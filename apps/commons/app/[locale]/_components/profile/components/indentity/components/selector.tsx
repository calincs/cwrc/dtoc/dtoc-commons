'use client';

import { useLoadIdentityProviders } from '@/app/_hooks/useLoadIdentityProviders';
import { Select } from '@mui/joy';
import { useSession } from 'next-auth/react';
import { Icon } from './icon';
import { Provider } from './provider';

interface Props {
  hover?: boolean;
}

export const Selector = ({ hover = false }: Props) => {
  const { data: session } = useSession();
  const { providers, isLoading, isError } = useLoadIdentityProviders();

  return (
    <Select
      disabled={isLoading || !!isError}
      name="identity-provider"
      size="sm"
      startDecorator={<Icon providerId={session?.user.preferredIdentityProvider} />}
      sx={{
        backgroundColor: hover ? undefined : 'transparent',
        '& .MuiSelect-button': { textTransform: 'capitalize' },
      }}
      value={session?.user.preferredIdentityProvider}
      variant={hover ? 'soft' : 'plain'}
    >
      {providers?.map(({ enabled = false, providerId }) => {
        if (!providerId) return null;
        return (
          <Provider
            key={providerId}
            disabled={!enabled}
            linked={session?.user.linkedAccounts?.some(
              ({ identityProvider }) => identityProvider === providerId,
            )}
            selected={session?.user.preferredIdentityProvider === providerId}
            providerId={providerId}
          />
        );
      })}
    </Select>
  );
};
