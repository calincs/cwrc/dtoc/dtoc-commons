'use client';

import { BiRefresh, BiSolidBadgeCheck } from 'react-icons/bi';
import { MdOutlineAddLink } from 'react-icons/md';

interface Props {
  status?: 'selected' | 'unlinked' | 'expired';
}

export const Status = ({ status }: Props) => {
  if (status === 'selected') return <BiSolidBadgeCheck />;
  if (status === 'unlinked') return <MdOutlineAddLink />;
  if (status === 'expired') return <BiRefresh />;
  return null;
};
