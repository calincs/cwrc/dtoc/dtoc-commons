'use client';

import { useLinkedAccounts } from '@/app/_hooks/useLinkedAccounts';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { enqueueSnackbar } from 'notistack';

/**
 * The `useProviders` function is a TypeScript hook that provides various utility functions for
 * managing linked accounts and tokens for a specific provider.
 * @param {string} provider - The `provider` parameter is a string that represents the identity
 * provider that we want to interact with. It is used in various functions to perform actions specific
 * to that provider, such as checking if it is linked, linking an account, refreshing tokens, etc.
 * @returns The `useProviders` function returns an object with the following properties:
 */
export const useProviders = (provider: string) => {
  const { data: session, update } = useSession();
  const t = useTranslations();

  const {
    isIDPTokenExpired,
    linkAccount,
    refreshAllTokens: refreshAllIDPTokens,
    refreshToken: refreshIDPTokens,
  } = useLinkedAccounts();

  /**
   * The function checks if a specific provider is linked to the user's account.
   * @param {string} provider - The `provider` parameter is a string that represents the identity
   * provider that we want to check if it is linked to the user's account.
   * @returns The function `isProviderLinked` returns a boolean value.
   */
  const isProviderLinked = (provider: string) => {
    return session?.user.linkedAccounts?.some(
      ({ identityProvider }) => identityProvider === provider,
    );
  };

  /**
   * The function `isTokenExpired` checks if an IDP token is expired.
   */
  const isTokenExpired = async () => await isIDPTokenExpired(provider);

  /**
   * The function `linkProvider` is an asynchronous function that links an account with a provider and
   * displays a success message if the linking is successful.
   * @returns The function `linkProvider` returns a boolean value indicating whether the account linking
   * was successful or not.
   */
  const linkProvider = async () => {
    const response = await linkAccount(provider);

    if (!response.success) {
      if (response.error === 'timeout') return;
      const message = response.error ? response.error : t('common.Something whent wrong');
      enqueueSnackbar(message, { variant: 'error' });
      return;
    }

    await update({ reason: 'linkAccount' });
    enqueueSnackbar(`${provider} ${t('user.linked')}`);

    return response.success;
  };

  /**
   * The function `refreshAllTokens` refreshes all IDP tokens and displays a notification for each provider.
   * @returns If `providers` is falsy (e.g. `null`, `undefined`, `false`, or an empty array), then
   * nothing is being returned. Otherwise, an array of objects is being returned.
   */
  const refreshAllTokens = async () => {
    const providers = await refreshAllIDPTokens();
    if (!providers) return;

    for (const provider of providers) {
      if (!(typeof provider.message === 'string')) continue;
      enqueueSnackbar(provider.message, { variant: provider.refreshed ? 'default' : 'error' });
    }

    await update({ reason: 'refreshIDPTokens' });
  };

  /**
   * The function `refreshToken` refreshes IDP tokens and displays a success message if the refresh is successful.
   * @returns The function `refreshToken` returns a boolean value indicating whether the IDP tokens were successfully refreshed.
   */
  const refreshToken = async () => {
    const response = await refreshIDPTokens(provider);

    if (!response.success) {
      if (response.error === 'timeout') return;
      const message = response.error ? response.error : t('common.Something whent wrong');
      enqueueSnackbar(message, { variant: 'error' });
      return;
    }

    await update({ reason: 'refreshIDPTokens' });
    enqueueSnackbar(`${provider} ${t('user.tokens refreshed')}`);

    return response.success;
  };

  return {
    isProviderLinked,
    isTokenExpired,
    linkProvider,
    refreshToken,
    refreshAllTokens,
  };
};
