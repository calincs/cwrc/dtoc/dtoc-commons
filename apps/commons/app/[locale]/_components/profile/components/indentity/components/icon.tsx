import { AiFillGithub, AiFillGitlab } from 'react-icons/ai';
import { SiOrcid } from 'react-icons/si';

export const Icon = ({ providerId }: { providerId?: string }) => {
  if (providerId === 'github') return <AiFillGithub style={{ marginRight: 2 }} />;
  if (providerId === 'gitlab') return <AiFillGitlab style={{ marginRight: 2 }} />;
  if (providerId === 'orcid') return <SiOrcid style={{ marginRight: 2 }} />;
  return null;
};
