import { Sheet } from '@mui/joy';
import { useLocale } from 'next-intl';
import { Suspense } from 'react';
import { ContentMdxRemote } from '../mdx';
import { mdxComponents } from './mdxComponents';
import { primary } from '../themeRegistry/colors';

const contentPath = `${process.env.NEXTAUTH_URL}/content/about`;

export const AboutContent = () => {
  const locale = useLocale();

  return (
    <Sheet
      sx={{
        py: { xs: 3, sm: 6 },
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Sheet
        sx={{
          maxWidth: 1000, m: '0 auto',
          backgroundColor: 'transparent'
        }}
      >
        <Suspense fallback={<>loading</>}>
          <ContentMdxRemote components={mdxComponents} locale={locale} path={contentPath} />
        </Suspense>
      </Sheet>
    </Sheet>
  );
};
