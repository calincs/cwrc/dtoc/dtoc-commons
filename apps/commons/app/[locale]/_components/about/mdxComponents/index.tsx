import { Stack } from '@mui/joy';
import type { MDXComponents } from 'mdx/types';
import { TeamMember } from './teamMember';
import { A, H1, H2, H3, LI, P, UL } from './typography';

export const mdxComponents: MDXComponents = {
  wrapper: ({ children }) => (
    <Stack p={{ xs: 2, sm: 0 }} spacing={4}>
      {children}
    </Stack>
  ),
  section: ({ children }) => (
    <Stack direction={{ xs: 'column', sm: 'row' }} gap={{ xs: 2, sm: 6 }}>
      {children}
    </Stack>
  ),
  body: ({ children }) => <div>{children}</div>,
  h1: ({ children }) => <H1>{children}</H1>,
  h2: ({ children }) => <H2>{children}</H2>,
  h3: ({ children }) => <H3>{children}</H3>,
  p: ({ children }) => <P>{children}</P>,
  li: ({ children }) => <LI>{children}</LI>,
  ul: ({ children }) => <UL>{children}</UL>,
  a: ({ children }) => <A>{children}</A>,
  team: ({ children }) => {
    return (
      <Stack
        direction="row"
        flexWrap="wrap"
        justifyContent={{ xs: 'center', sm: 'flex-start' }}
        gap={3}
      >
        {children}
      </Stack>
    );
  },
  team_member: (props) => {
    if (!props.name || !props.position) return null;
    return <TeamMember {...props} />;
  },
};
