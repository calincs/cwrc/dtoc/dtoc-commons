'use client';

import { Link, Sheet, Stack, Typography } from '@mui/joy';
import { motion } from 'framer-motion';
import Image from 'next/image';

export interface Props {
  avatar?: string;
  date?: string;
  gravatar?: string;
  name: string;
  link?: string;
  position: string;
}

const imageLoader = ({ src }: { src: string }) => src;

const imageLoaderGravatar = ({ src, width }: { src: string; width: number }) => {
  return `https://www.gravatar.com/avatar/${src}?d=retro&s=${width}}`;
};

export const TeamMember = ({ avatar, date, gravatar: gravatar, link, name, position }: Props) => {
  return (
    <Stack direction="row" gap={0.5}>
      <Sheet
        color="primary"
        sx={{ width: 48, height: 48, borderRadius: ({ radius }) => radius.sm, overflow: 'hidden' }}
      >
        <Image
          loader={avatar ? imageLoader : imageLoaderGravatar}
          src={gravatar ?? name}
          width={48}
          height={48}
          alt={name}
        />
      </Sheet>
      <Stack
        component={motion.div}
        whileHover={{ y: -4 }}
        justifyContent="center"
        sx={{
          height: 48,
          overflow: 'hidden',
          borderRadius: ({ radius }) => radius.sm,
          boxShadow: ({ shadow }) => shadow.sm,
        }}
      >
        <Stack height={26} justifyContent="center">
          {link ? (
            <Link
              color="neutral"
              fontWeight={700}
              href={link ?? ''}
              level="body-sm"
              px={1.7}
              py={0.4}
              target="_blank"
              variant="soft"
            >
              {name}
            </Link>
          ) : (
            <Typography fontWeight={700} level="body-sm" px={1.7} py={0.4} variant="soft">
              {name}
            </Typography>
          )}
        </Stack>
        <Typography level="body-xs" px={1.25}>
          {position}
        </Typography>
      </Stack>
    </Stack>
  );
};
