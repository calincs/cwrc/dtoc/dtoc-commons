'use client';
import { Link, Typography } from '@mui/joy';
import { ReactNode } from 'react';

export const H1 = ({ children }: { children: ReactNode }) => (
  <Typography
    component="h1"
    level="h1"
    sx={{
      minWidth: 200,
      color: ({ palette }) => (palette.mode === 'dark' ? 'primary.400' : 'primary'),
    }}
    textAlign={{ xs: 'center', sm: 'right' }}
  >
    {children}
  </Typography>
);

export const H2 = ({ children }: { children: ReactNode }) => (
  <Typography
    component="h2"
    level="h2"
    sx={{
      minWidth: 200,
      color: ({ palette }) => (palette.mode === 'dark' ? 'primary.400' : 'primary'),
    }}
    textAlign={{ xs: 'center', sm: 'right' }}
  >
    {children}
  </Typography>
);
export const H3 = ({ children }: { children: ReactNode }) => (
  <Typography
    component="h3"
    level="h3"
    sx={{
      minWidth: 200,
      color: ({ palette }) => (palette.mode === 'dark' ? 'primary.600' : 'primary'),
    }}
    pb={1}
  >
    {children}
  </Typography>
);

export const P = ({ children }: { children: ReactNode }) => (
  <Typography level="body-sm" pb={1}>
    {children}
  </Typography>
);

export const UL = ({ children }: { children: ReactNode }) => (
  <ul style={{ marginBlock: 0 }}>{children}</ul>
);

export const LI = ({ children }: { children: ReactNode }) => (
  <li
    style={{
      fontFamily: `var(--joy-fontFamily-body, "Inter", var(--joy-fontFamily-fallback, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"))`,
      fontSize: 'var(--Typography-fontSize, var(--joy-fontSize-sm, 0.875rem))',
      lineHeight: `var(--joy-lineHeight-md, 1.5)`,
      color: `var(--joy-palette-text-tertiary, var(--joy-palette-neutral-600, #555E68))`,
    }}
  >
    {children}
  </li>
);

export const A = ({ children, ...props }: { children: ReactNode }) => (
  <Link
    color="neutral"
    rel="noreferrer"
    target="_blank"
    underline="always"
    {...(props as HTMLLinkElement['attributes'])}
  >
    {children}
  </Link>
);
