import type { MDXComponents } from 'mdx/types';
import { MDXRemote } from 'next-mdx-remote/rsc';
import remarkDirective from 'remark-directive';
import remarkDirectiveRehype from 'remark-directive-rehype';

interface Props {
  components?: MDXComponents;
  locale?: string;
  path: string;
}

export const ContentMdxRemote = async ({ components, locale = 'en-CA', path }: Props) => {
  const res = await fetch(`${path}/${locale}.mdx`);
  const markdown = await res.text();
  return (
    <MDXRemote
      options={{ mdxOptions: { remarkPlugins: [remarkDirective, remarkDirectiveRehype] } }}
      source={markdown}
      components={components}
    />
  );
};
