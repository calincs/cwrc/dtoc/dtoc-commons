'use client';

import { usePathname, useRouter } from '@/app/navigation';
import { Option, Select, VariantProp } from '@mui/joy';
import { useLocale, useTranslations } from 'next-intl';
import { CSSProperties, useTransition } from 'react';

interface Props {
  backgroundColor?: CSSProperties['color'];
  variant?: VariantProp;
}

export const LocaleSwitcher = ({ backgroundColor, variant = 'plain' }: Props) => {
  const locale = useLocale();
  const router = useRouter();
  const pathname = usePathname();
  const t = useTranslations();

  const [isPending, startTransition] = useTransition();

  const onSelectChange = (nextLocale: typeof locale) => {
    startTransition(() => {
      router.replace(pathname, { locale: nextLocale });
    });
  };

  return (
    <Select
      disabled={isPending}
      name="locale"
      onChange={(event, newValue) => {
        if (!newValue) return;
        onSelectChange(newValue);
      }}
      size="sm"
      sx={{ backgroundColor }}
      value={locale}
      variant={variant}
    >
      {['en-CA', 'fr-CA'].map((locale) => (
        <Option
          key={locale}
          sx={{ mx: 0.5, borderRadius: 'sm' }}
          value={locale}
          // label={t('LocaleSwitcher.localeShort', { locale: locale.replace('-', '_') })}
        >
          {t('LocaleSwitcher.locale', { locale: locale.replace('-', '_') })}
        </Option>
      ))}
    </Select>
  );
};
