'use client';

import { IconButton, ToggleButtonGroup } from '@mui/joy';
import { useColorScheme } from '@mui/joy/styles';
import { useColorScheme as useMaterialColorScheme } from '@mui/material/styles';
import { useEffect, useState } from 'react';
import { FiMonitor } from 'react-icons/fi';
import { MdDarkMode, MdLightMode } from 'react-icons/md';

interface Props {
  bordered?: boolean;
}

export const ThemeSwitcher = ({ bordered = false }: Props) => {
  const { mode, setMode, systemMode } = useColorScheme();
  const { mode: materialMode, setMode: setMaterialMode } = useMaterialColorScheme();

  const [isDark, setIsDark] = useState(false);
  const [value, setValue] = useState<typeof mode | null>(null);

  useEffect(() => {
    const isDark = mode === 'dark' || (mode === 'system' && systemMode === 'dark');
    setValue(mode);
    setIsDark(isDark);
    setMaterialMode(isDark ? 'dark' : 'light');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mode, systemMode]);

  return (
    <ToggleButtonGroup
      aria-label="theme switcher"
      color={isDark ? 'primary' : 'neutral'}
      onChange={(event, newValue) => {
        if (!newValue) return;
        setValue(newValue);
        setMode(newValue);
        setMaterialMode(materialMode === 'dark' ? 'light' : 'dark');
      }}
      size="sm"
      spacing={0.5}
      value={value}
      variant="plain"
      sx={{
        p: 0.25,
        borderRadius: bordered ? ({ spacing }) => spacing(4) : 4,
        borderColor: ({ palette }) => (isDark ? palette.primary[700] : palette.neutral[400]),
        borderStyle: 'solid',
        borderWidth: bordered ? 0.5 : 0,
        '--ButtonGroup-connected': '0',
        '&:hover': {
          borderColor: ({ palette }) => (isDark ? palette.primary[500] : palette.neutral[700]),
        },
      }}
    >
      <IconButton aria-label="light" value="light">
        <MdLightMode />
      </IconButton>
      <IconButton aria-label="system" value="system">
        <FiMonitor />
      </IconButton>
      <IconButton aria-label="dark" value="dark">
        <MdDarkMode />
      </IconButton>
    </ToggleButtonGroup>
  );
};
