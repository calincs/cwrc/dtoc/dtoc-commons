import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { type PropsWithChildren } from 'react';

interface Props extends PropsWithChildren {
  isRefresh?: boolean;
}

export const SuccessMessage = ({ children, isRefresh = false }: Props) => {
  const t = useTranslations();

  return (
    <Typography color="success" variant="soft">
      <Typography textTransform="capitalize">{children}</Typography>
      <Typography>
        {isRefresh ? ` ${t('ServicePage.tokens refreshed')}` : ` ${t('ServicePage.linked')}`}
      </Typography>
    </Typography>
  );
};
