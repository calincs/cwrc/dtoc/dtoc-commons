import { Sheet, Stack, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Logo } from '../../_components';
import { Broadcast, ErrorMessage } from '../_components';
import { SuccessMessage } from './success';
import { primary } from '../../_components/themeRegistry/colors';

export default function LinkAccounts({
  searchParams,
}: {
  searchParams: Record<string, string | string[] | undefined>;
}) {
  const t = useTranslations();

  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;
  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  const provider = Array.isArray(searchParams.provider)
    ? searchParams.provider[0]
    : searchParams.provider;

  const isRefresh = Array.isArray(searchParams.isRefresh)
    ? searchParams.isRefresh[0]
    : searchParams.isRefresh;

  return (
    <Sheet
      component="main"
      sx={{
        height: '100vh',
        width: '100vw',
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Stack alignItems="center" gap={3} justifyContent="center" sx={{ height: '50vh' }}>
        <Logo width={325} />
        <Typography level="h3">
          {isRefresh === 'true' ? t('ServicePage.Refresh Tokens') : t('ServicePage.Linked Account')}
        </Typography>
        {provider && <SuccessMessage isRefresh={isRefresh === 'true'}>{provider}</SuccessMessage>}
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <Broadcast
          channel="keycloak-link-account"
          isRefresh={isRefresh === 'true' ? true : false}
          {...{ codeVerifier, error, provider }}
        />
      </Stack>
    </Sheet>
  );
}
