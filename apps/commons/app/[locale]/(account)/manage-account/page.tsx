import { Sheet, Stack, Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { Logo } from '../../_components';
import { Broadcast, ErrorMessage } from '../_components';
import { primary } from '../../_components/themeRegistry/colors';

export default function LinkAccounts({
  searchParams,
}: {
  searchParams: Record<string, string | string[] | undefined>;
}) {
  const t = useTranslations();

  const codeVerifier = Array.isArray(searchParams.codeVerifier)
    ? searchParams.codeVerifier[0]
    : searchParams.codeVerifier;
  const error = Array.isArray(searchParams.error) ? searchParams.error[0] : searchParams.error;

  return (
    <Sheet
      component="main"
      sx={{
        height: '100vh',
        width: '100vw',
        background: `linear-gradient(180deg, ${primary['50']} 0%, ${primary['100']} 100%)`,
      }}
    >
      <Stack alignItems="center" gap={3} justifyContent="center" sx={{ height: '50vh' }}>
        <Logo width={325} />
        <Typography level="h3">{t('ServicePage.Account Manage callback')}</Typography>
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <Broadcast channel="keycloak-account-manager" {...{ codeVerifier, error }} />
      </Stack>
    </Sheet>
  );
}
