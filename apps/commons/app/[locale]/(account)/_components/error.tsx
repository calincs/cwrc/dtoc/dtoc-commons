'use client';

import { Typography } from '@mui/joy';
import { useTranslations } from 'next-intl';
import { type PropsWithChildren } from 'react';

export const ErrorMessage = ({ children }: PropsWithChildren) => {
  const t = useTranslations();

  return (
    <>
      <Typography level="h4" textTransform="capitalize">
        {t('common.error')}
      </Typography>
      <Typography color="danger" variant="soft">
        {children}
      </Typography>
    </>
  );
};
