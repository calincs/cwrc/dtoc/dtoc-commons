'use client';

import { Button } from '@mui/joy';
import { BroadcastChannel } from 'broadcast-channel';
import 'client-only';
import { useTranslations } from 'next-intl';
import { useEffect } from 'react';

type Channel = 'keycloak-account-manager' | 'keycloak-link-account';

interface Props {
  channel: Channel;
  codeVerifier?: string;
  error?: string;
  isRefresh?: boolean;
  provider?: string;
}

export const Broadcast = (props: Props) => {
  const t = useTranslations();

  const success = !props.error;

  useEffect(() => {
    const channel = new BroadcastChannel(props.channel);
    const message = { success, ...props };
    void channel.postMessage(message);
    window.close();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = () => window.close();

  return (
    <Button onClick={handleClick} variant="plain">
      {t('ServicePage.Close this window')}
    </Button>
  );
};
