import { primary } from "../_components/themeRegistry/colors";

export default async function Layout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <main style={{ height: '100vh', width: '100vw', backgroundColor: primary['200'] }}>
      {children}
    </main>
  );
}
