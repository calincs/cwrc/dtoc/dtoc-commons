'use client';

import { useEffect, useState, memo } from 'react';
import { Typography } from '@mui/joy';
import { getDtocApp } from '../../helperDtoc';
import { DToCConfig } from '@/types';
import { useAtomValue } from 'jotai';
import { commonsDocumentAtom } from '@/jotai/store';
import { primary } from '../../../_components/themeRegistry/colors';

export const Title = memo(function Title() {
  const commonsDocument = useAtomValue(commonsDocumentAtom);

  const [_title, _setTitle] = useState('');
  const [_subTitle, _setSubTitle] = useState('');

  const doGetTitle = async () => {
    console.log('doGetTitle');
    const dtoc = await getDtocApp();
    if (!dtoc) return 'Untitled';
    const inputConfig = dtoc.getInputConfig() as DToCConfig;
    // console.log('inputConfig', inputConfig)
    let title = inputConfig.editionTitle;
    // console.log('commonsDocument', commonsDocument)
    if (!title && commonsDocument && 'filename' in commonsDocument.source) {
      title = commonsDocument.source.filename;
    }
    if (!title) title = 'Untitled';
    _setTitle(title);
    let subTitle = inputConfig.editionSubtitle;
    if (!subTitle) subTitle = '';
    _setSubTitle(subTitle);
  };

  const addTitleListener = async () => {
    console.log('addTitleListener');
    const dtoc = await getDtocApp();
    if (!dtoc) return;
    dtoc.subscribe('loadedCorpus', doGetTitle);
  };

  const removeTitleListener = async () => {
    console.log('removeTitleListener');
    const dtoc = await getDtocApp();
    if (!dtoc) return;

    try {
      dtoc.unsubscribe('loadedCorpus', doGetTitle);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    addTitleListener();
    if (process.env.NODE_ENV === 'production') {
      // this causes doc load issues on development
      return () => {
        removeTitleListener();
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    doGetTitle();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [commonsDocument?.lastOpenedAt]);

  return (
    <>
      <Typography level="h4" textColor={primary['50']} letterSpacing={1}>
        {_title}
      </Typography>
      {_subTitle !== '' ? (
        <Typography level="h4" fontWeight={300} textColor={primary['50']} letterSpacing={1}>
          &nbsp;&nbsp;&nbsp;{_subTitle}
        </Typography>
      ) : (
        ''
      )}
    </>
  );
});
