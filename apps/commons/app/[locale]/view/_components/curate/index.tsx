import { Button } from "@mui/joy"
import { useState } from "react";
import { PiPencilFill } from "react-icons/pi";
import { getDtocApp } from "../../helperDtoc";

export const CurateButton = () => {
    const [isCurator, setIsCurator] = useState(false);

    const onCurate = async () => {
        const dtocApp = await getDtocApp();
        if (!dtocApp) return;

        setIsCurator(!dtocApp.isCurator);
        if (dtocApp.isCurator) {
            dtocApp.showDefaultView();
        } else {
            dtocApp.showEditView();

        }
    }
    
    return (
        <Button
            startDecorator={<PiPencilFill/>}
            // aria-pressed={isCurator}
            onClick={() => onCurate()}
            size="sm"
            sx={{
                marginRight: '8px'
            }}
            // sx={(theme) => ({
            //   [`&[aria-pressed="true"]`]: {
            //     ...theme.variants.outlinedActive.neutral,
            //     borderColor: theme.vars.palette.neutral.outlinedHoverBorder,
            //   },
            // })}
            variant="soft"
            >
            {isCurator ? "Done Curation" : "Curate Tags"}
        </Button>
    )
}