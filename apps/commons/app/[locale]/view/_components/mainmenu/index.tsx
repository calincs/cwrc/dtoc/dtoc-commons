'use client';

import { Storage } from '@/app/_components/dialogs';
import { Link } from '@/app/navigation';
import { ListDivider, Dropdown, IconButton, Menu, MenuButton, MenuItem, Stack } from '@mui/joy';
import { GiHamburgerMenu } from 'react-icons/gi';
import { useMainMenu } from './_hooks/useMainMenu';
import { primary } from '../../../_components/themeRegistry/colors';

export const MainMenu = () => {
  const { onNew, onEdit, onOpen, onSave, onSaveAs, onClose, loading, isSavedAs, handleCancel, handleCloseLoadStorage, handleCloseSaveStorage } = useMainMenu();

  return (
    <Stack direction="row">
      <Dropdown>
        <MenuButton
          slots={{ root: IconButton }}
          slotProps={{ root: { variant: 'plain', sx: {color: primary['50'], ':hover': { color: primary['50'], backgroundColor: primary['600'] }} } }}
        >
          <GiHamburgerMenu />
        </MenuButton>

        <Menu
          placement="bottom-start"
          size="sm"
          sx={{ minWidth: 180 }}
          variant='plain'
        >
          <MenuItem onClick={onNew}>
            New
          </MenuItem>
          <ListDivider />
          <MenuItem onClick={onOpen}>
            Open
          </MenuItem>
          <ListDivider />
          <MenuItem onClick={onEdit}>
            Edit
          </MenuItem>
          <ListDivider />
          <MenuItem onClick={onSave}>
            Save
          </MenuItem>
          <MenuItem onClick={onSaveAs}>
            Save as
          </MenuItem>
          <ListDivider />
          <Link
            href="https://cwrc.ca/Documentation/public/DITA_Files-Various_Applications/DToC/OverviewDToC.html"
            target='_blank'
            style={{ textDecoration: 'none' }}
          >
            <MenuItem>
              Help
            </MenuItem>
          </Link>
          <ListDivider />
          <Link href="/" passHref style={{ width: '100%', textDecoration: 'none' }}>
            <MenuItem onClick={onClose}>
              Close
              </MenuItem>
          </Link>
        </Menu>
      </Dropdown>
      {loading && !isSavedAs && <Storage onCancel={handleCancel} onClose={handleCloseLoadStorage} type="load" />}
      {isSavedAs && !loading && <Storage onCancel={handleCancel} onClose={handleCloseSaveStorage} source='cloud' type="save" />}
    </Stack>
  );
};
