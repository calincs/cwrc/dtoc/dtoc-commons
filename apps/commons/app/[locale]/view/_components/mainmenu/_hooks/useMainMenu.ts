import { useSession } from 'next-auth/react';
import { useFile, useLinkedAccounts } from '@/app/_hooks';
import { useState } from 'react';
import { useAtom, useAtomValue } from 'jotai';
import { useRouter } from 'next/navigation';
import { v4 as uuidv4 } from 'uuid';
import { CommonsRecent, db } from '@/db';
import { Resource, saveDocument } from '@cwrc/leafwriter-storage-service/headless';
import { commonsDocumentAtom, resourceAtom, baseUrlAtom } from '@/jotai/store';
import { enqueueSnackbar } from 'notistack';
import { getDtocApp } from '../../../helperDtoc';

export const useMainMenu = () => {

    const router = useRouter();
    const { data: session } = useSession();
    const { isIDPTokenExpired } = useLinkedAccounts();
    const { parseResource } = useFile();

    const [isSavedAs, setIsSavedAs] = useState(false);
    const [loading, setLoading] = useState(false);

    const [commonsDocument, setCommonDocument] = useAtom(commonsDocumentAtom);
    const [resource, setResource] = useAtom(resourceAtom);
    const baseUrl = useAtomValue(baseUrlAtom);

    const getProvider = async (provider: string) => {
        if (!session?.user.linkedAccounts) return;

        const account = session?.user.linkedAccounts?.find((account) => account.identityProvider === provider);
        if (!account) return;

        const isExpired = await isIDPTokenExpired(account.identityProvider);
        if (isExpired) return;

        const providerAuth = {
            name: account.identityProvider,
            access_token: account.access_token!,
        };

        console.log('providerAuth', providerAuth);

        return providerAuth;
    };

    const onNew = () => {
        window.open(`${baseUrl}/view`)
        // router.push('/view');
    };

    const onEdit = async () => {
        const dtocApp = await getDtocApp();
        const config = dtocApp.getInputConfig();
        dtocApp.showInputWindow(config);
    };

    const onOpen = () => {
        setLoading(true);
    };

    const onSave = async () => {
        if (!commonsDocument) return;
        if (!('repository' in commonsDocument.source)) {
            onSaveAs();
            return;
        }

        const providerAuth = await getProvider(commonsDocument.source.repository.provider);
        if (!providerAuth) {
            onSaveAs();
            return;
        };

        const dtocApp = await getDtocApp();
        const config = dtocApp.getInputConfig();
        if (!config) return;

        const repository = commonsDocument.source.repository;

        const updatedContent: Resource = {
            provider: repository.provider,
            ownerType: repository.ownerType,
            owner: repository.owner,
            repo: repository.repo,
            path: repository.path,
            filename: repository.filename,
            content: JSON.stringify(config, null, 4),
        }

        const response = await saveDocument(providerAuth, updatedContent, true);
        if ('message' in response) {
            enqueueSnackbar(`Your document settings were not saved:\n${response.message}`, { variant: 'error' });
        } else {
            enqueueSnackbar('Your document settings were saved', { variant: 'success' });
        }
    };

    const onSaveAs = async () => {
        if (!commonsDocument) return;

        const dtocApp = await getDtocApp();
        const config = dtocApp.getInputConfig();
        if (!config) return;

        let newResource: Resource = {
            content: JSON.stringify(config, null, 4),
        }

        if ('repository' in commonsDocument.source) {
            const repository = commonsDocument.source.repository;
            newResource = {
                ...newResource,
                provider: repository.provider,
                ownerType: repository.ownerType,
                owner: repository.owner,
                repo: repository.repo,
                path: repository.path,
                filename: repository.filename,
            }
        };

        console.log('newResource', newResource);

        setResource(newResource)
        setIsSavedAs(true);
    };

    const handleCloseLoadStorage = async (resource?: Resource) => {
        console.log('handleCloseLoadStorage', resource)
        if (!resource) {
            setLoading(false);
            return;
        }

        const id = await parseResource(resource);
        if (!id) {
            setLoading(false);
            return;
        }

        setLoading(false);
        const params = new URLSearchParams({ document: id });
        window.open(`${baseUrl}/view?${params}`);
    };

    const handleCloseSaveStorage = async (resource?: Resource) => {
        if (!resource) {
            enqueueSnackbar(`Your document settings were not saved`, { variant: 'error' });
            return;
        }

        console.log('resource', resource);

        const newCommonsDocument: CommonsRecent = {
            id: uuidv4(),
            lastOpenedAt: new Date(),
            source: {
                filename: resource.filename ?? 'untitled',
                url: resource.url!,
                content: resource.content,
                repository: {
                    provider: resource.provider!,
                    ownerType: resource.ownerType!,
                    owner: resource.owner!,
                    repo: resource.repo!,
                    path: resource.path!,
                    filename: resource.filename!,
                    branch: resource.branch ?? 'main',
                }
            }
        }

        setCommonDocument(newCommonsDocument);
        await db.recentDocuments.add(newCommonsDocument);

        enqueueSnackbar('Your document settings were saved', { variant: 'success' });

        setIsSavedAs(false);
    };

    const onClose = () => {
        setCommonDocument(undefined);
    }

    const handleCancel = () => {
        setLoading(false);
        setIsSavedAs(false);
    }

    return {
        onNew,
        onEdit,
        onOpen,
        onSave,
        onSaveAs,
        onClose,
        loading,
        isSavedAs,
        handleCancel,
        handleCloseLoadStorage,
        handleCloseSaveStorage
    }

}