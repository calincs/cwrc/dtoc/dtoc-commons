import { splitPathFilename } from '@/app/_util';
import {
  AnnotationCollectionSchema,
  type AnnotationCollection,
  type CommonsDocument,
  type Repository,
  DToCConfigSchema,
} from '@/types';
import { v4 as uuidv4 } from 'uuid';
import { z } from 'zod';
import type { PageSearchParams } from './page';
import { CommonsRecent } from '@/db';

const UUIDSchema = z.string().uuid();

interface Response {
  id?: string;
  commonsDocument?: CommonsDocument;
}

export const loadFile = async (id?: string): Promise<Response | undefined> => {
  if (!id) return;
  
  const isUUID = UUIDSchema.safeParse(id);
  if (isUUID.success) return { id };
  
  return loadDocument(id);
};

const getRepositoryFromUrl = (urlString: string) => {
  const url = new URL(urlString);
  if (url.hostname === 'raw.githubusercontent.com') return getGithubRepositoryFromUrl(url);
  if (url.hostname === 'gitlab.com' && !url.pathname.startsWith('/api')) {
    return getGitlabRepositoryFromUrl(url);
  }
};

const loadDocument = async (documentUrl: string) => {
  const documentContent = await fetch(documentUrl);
  if (!documentContent.ok) return;

  const data = await documentContent.json();
  console.log('loadDocument', data)

  const isDtocConfig = DToCConfigSchema.safeParse(data)
  if (!isDtocConfig.success) return;

  const config = isDtocConfig.data;

  const [, documentFilename] = splitPathFilename(documentUrl);
  const repository = getRepositoryFromUrl(documentUrl);

  const commonsDocument: CommonsDocument = {
    source: {
      url: documentUrl,
      content: JSON.stringify(config),
      filename: repository?.filename ?? documentFilename ?? 'untitled',
      repository,
    },
  };

  return { commonsDocument };
};

const getGithubRepositoryFromUrl = (url: URL): Repository | undefined => {
  // * ex: 'https://raw.githubusercontent.com/lucaju/cwrc-writer-samples/master/samples/Blank.xml"

  const pathArray = url.pathname.split('/');
  pathArray.shift(); // Remove first item: URL Path always starts with '/', which makes the first item be empty

  const owner = pathArray.shift();
  if (!owner) return;

  const repo = pathArray.shift();
  if (!repo) return;

  const branch = pathArray.shift();
  if (!branch) return;

  const filename = pathArray.pop(); // filename is always the last item.

  //* the remaining items are part of the path inside the repositpty

  return {
    provider: 'github',
    ownerType: 'user', //? assumption
    owner,
    repo,
    branch,
    path: pathArray.join('/'),
    filename: filename ? decodeURI(filename) : `untitled-${uuidv4}`,
  };
};

const getGitlabRepositoryFromUrl = (url: URL): Repository | undefined => {
  // * Ex: via GUI: 'https://gitlab.com/lucaju/CiteLens/-/raw/master/src/mvc/letter.xml?ref_type=heads"

  //! not supported: via API: https://gitlab.com/api/v4/projects/${repoId}/repository/files/${encodedPath}/raw
  if (url.pathname.startsWith('/api')) return;

  const pathArray = url.pathname.split('/');
  pathArray.shift(); // Remove first item: URL Path always starts with '/', which makes the first item be empty

  const owner = pathArray.shift();
  if (!owner) return;

  const repo = pathArray.shift();
  if (!repo) return;

  pathArray.shift(); // remove '-'
  pathArray.shift(); // remove 'raw'

  const branch = pathArray.shift();
  if (!branch) return;

  const filename = pathArray.pop(); // filename is always the last item.

  //* the remaining items are part of the path inside the repositpty

  return {
    provider: 'gitlab',
    ownerType: 'user', //? assumption
    owner,
    repo,
    branch,
    path: pathArray.join('/'),
    filename: filename ? decodeURI(filename) : `untitled-${uuidv4}`,
  };
};
