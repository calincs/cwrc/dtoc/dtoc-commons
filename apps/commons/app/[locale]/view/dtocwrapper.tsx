'use client';

import { CommonsDocument } from '@/types';
import { useDtocFile } from './_hooks/useDtocFile';
import { useEffect, memo } from 'react';
import { Logo, Profile, TopBar } from '../_components';
import { MainMenu } from './_components/mainmenu';
import { Title } from './_components/title';
import { DevTools } from 'jotai-devtools';
import { useAtom, useSetAtom } from 'jotai';
import { commonsDocumentAtom, baseUrlAtom } from '@/jotai/store';
import type { Session } from 'next-auth';
import { Stack, Typography } from '@mui/joy';
import { CurateButton } from './_components/curate';
import { primary } from '../_components/themeRegistry/colors';

declare global {
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    dtocApp: any;
  }
}

interface Props {
  baseUrl: string;
  newDoc?: boolean;
  id?: string;
  commonsDocument?: CommonsDocument;
  dtocRootUrl: string;
  session: Session | null;
}

export const DToCWrapper = (props: Props) => {
  const { session, baseUrl, ...rest } = props;
  const { newDocument, processDocument } = useDtocFile();
  console.log('--------- DToCWrapper RERENDER');

  const [_commonsDocument, _setCommonsDocument] = useAtom(commonsDocumentAtom);
  const setBaseUrl = useSetAtom(baseUrlAtom);

  useEffect(() => {
    setBaseUrl(baseUrl);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    console.log('--------------dtocWrapper useEffect!!!');
    if (!_commonsDocument) {
      if (props.newDoc) {
        newDocument();
      } else {
        console.log('dtocWrapper useEffect processDocument');
        processDocument({ id: props.id, commonsDocument: props.commonsDocument });
      }
    }
    return () => {
      console.log('useEffect CLEAN UP');
      // _setCommonsDocument(undefined)
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Stack
      direction="column"
      justifyContent="flex-start"
      alignItems="stretch"
      spacing={0}
      sx={{ height: '100vh' }}
    >
      <TopBar
        left={
          <Stack direction="row" justifyContent="flex-start" alignItems="center" spacing={2}>
            <MainMenu />
            <Stack direction="row" spacing={1} sx={{ color: primary['50'] }}>
              <Logo height={32} invertColor />
              <Typography
                textColor={primary['50']}
                fontWeight={600}
                fontSize="14px"
                margin="auto 8px auto 8px !important"
              >
                DToC
              </Typography>{' '}
              {/* TODO hardcoded styles */}
            </Stack>
          </Stack>
        }
        center={<Title />}
        right={
          <>
            <CurateButton />
            {session && <Profile session={session} />}
          </>
        }
      />
      {/* <DevTools /> */}
      <iframe
        src={props.dtocRootUrl}
        style={{ border: '0px', minHeight: 0, flexGrow: 1 }}
        name="dtocWrapperFrame"
      />
    </Stack>
  );
};
