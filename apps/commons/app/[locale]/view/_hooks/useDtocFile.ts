import { db, type CommonsRecent } from '@/db';
import { DToCConfig, type CommonsDocument } from '@/types';
import { useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useAtom } from 'jotai';
import { commonsDocumentAtom } from '@/jotai/store';
import { getDtocApp } from '../helperDtoc';

interface Props {
    id?: NonNullable<CommonsDocument['id']>;
    commonsDocument?: CommonsDocument;
    newDoc?: boolean;
}

export const useDtocFile = () => {
    const [_commonsDocument, _setCommonsDocument] = useAtom(commonsDocumentAtom);

    const processDocument = async ({ id, commonsDocument }: { id?: string, commonsDocument?: CommonsDocument }) => {
        // console.log('processDocument getDtocApp');
        const dtocApp = await getDtocApp();
        if (!dtocApp) return;

        // console.log('processDocument', id);
        let _id = id;

        if (commonsDocument) {

            if ('url' in commonsDocument.source) {
                const existingDocument = await getRecentByDocumentUrl(commonsDocument.source.url);
                if (existingDocument) {
                    _id = await updateDb(existingDocument);
                } else {
                    _id = await addToDb(commonsDocument);
                }
            }

        }

        if (!_id) return;


        // console.log('processDocument dtocId', _id);
        const document = await db.recentDocuments.get(_id);
        if (!document) return;

        const config = getConfigFromDocument(document)
        if (!config) return;

        if ('load' in dtocApp) {
            // console.log('processDocument load config', config);
            dtocApp.load(config);
        }

        // console.log('processDocument _setCommonsDocument');
        _setCommonsDocument(document);
    };

    const newDocument = async () => {
        console.log('newDocument getDtocApp');

        const dtocApp = await getDtocApp();
        if (!dtocApp) return;

        _setCommonsDocument({
            source: {
                content: ''
            },
            id: uuidv4(),
            lastOpenedAt: new Date(),
        });

        dtocApp.showInputWindow();
    }

    // useEffect(() => {
    //     if (newDoc) {
    //         newDocument();
    //         return;
    //     }
    //     processDocument();
    // }, [_commonsDocument?.id]);

    const addToDb = async (commonsDocument: CommonsDocument) => {
        const recent: CommonsRecent = {
            ...commonsDocument,
            id: uuidv4(),
            lastOpenedAt: new Date(),
        }
        return await db.recentDocuments.add(recent);
    }

    const getRecentbyId = async (id: string) => {
        return await db.recentDocuments.get(id);
    };

    const getRecentByDocumentUrl = async (url: string) => {
        // return (await db.recentDocuments.where('source.url').equals(url).toArray())[0];
        return await db.recentDocuments.get({ 'source.url': url });
    };

    const updateDb = async (commonsDocument: CommonsRecent) => {
        const affected = await db.recentDocuments.update(commonsDocument.id, {
            lastOpenedAt: new Date(),
        });
        if (affected === 0) {
            await db.recentDocuments.put(commonsDocument, commonsDocument.id);
        }

        return commonsDocument.id;
    };

    const getConfigFromDocument = (document: CommonsRecent) => {
        const config = document.source.content;
        if (!config) return;

        try {
            const dtocConfig: DToCConfig = JSON.parse(config);
            return dtocConfig;
        } catch {
            return
        }
    }

    return {
        newDocument,
        processDocument,
    }
};