import { Metadata } from 'next';
import { DToCWrapper } from './dtocwrapper';
import { loadFile } from './helperFile';
import { getServerSession } from 'next-auth';
import { authOptions } from '@/app/api/lincs/_auth';

export interface PageSearchParams {
  document?: string;
};

export const metadata: Metadata = {
  title: 'DToC',
};

export default async function Edit({
  searchParams,
}: {
  // searchParams: Record<string, string | string[] | undefined>;
  searchParams: PageSearchParams;
}) {

  const session = await getServerSession(authOptions);
  const isNew = !searchParams.document;
  const requestFile = await loadFile(searchParams.document);
  const dtocRootUrl = `/dtoc/index.html?dtocRootUrl=${process.env.NEXTAUTH_URL}${process.env.DTOC_ROOT}`;

  return (
    <DToCWrapper
      baseUrl={process.env.NEXTAUTH_URL}
      dtocRootUrl={dtocRootUrl}
      id={requestFile?.id}
      commonsDocument={requestFile?.commonsDocument}
      newDoc={isNew}
      session={session}
    />
  );
}
