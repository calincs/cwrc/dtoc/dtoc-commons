// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getDtocApp = (): Promise<any> => {
    return new Promise((resolve) => {
        const doGet = (count: number) => {
            if (count >= 500) {
                // TODO throw error?
                resolve(undefined);
                return;
            }
            const namedEl = window.document.getElementsByName('dtocWrapperFrame')[0];
            if (!namedEl) {
                resolve(undefined);
                return;
            } else {
                const frame = namedEl as HTMLIFrameElement;
                if (frame.contentWindow?.dtocApp) {
                    // console.log('got app', count);
                    resolve(frame.contentWindow.dtocApp)
                } else {
                    // console.log('did not get app', count);
                    setTimeout(() => doGet(count+1), 100);
                }
            }
        }
        doGet(0);
    });
}