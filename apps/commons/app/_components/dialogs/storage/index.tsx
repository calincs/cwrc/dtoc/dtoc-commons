'use client';

import { useLinkedAccounts, usePermalink } from '@/app/_hooks';
import { usePathname } from '@/app/navigation';
import { resourceAtom } from '@/jotai/store';
import { Resource } from '@cwrc/leafwriter-storage-service/headless';
import { DToCConfigSchema } from '@/types/dtocDocument';
import {
  StorageDialog,
  type DialogType,
  type ProviderAuth,
  type StorageSource,
} from '@cwrc/leafwriter-storage-service/dialog';
import { useAtom } from 'jotai';
import { useResetAtom } from 'jotai/utils';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useEffect, useState } from 'react';

interface Props {
  label?: string;
  onCancel?: () => void;
  onClose?: (resource?: Resource) => void;
  source?: StorageSource;
  type?: DialogType;
}

export const Storage = ({ label, onCancel, onClose, source = 'url', type = 'load' }: Props) => {
  const { isIDPTokenExpired } = useLinkedAccounts();
  const pathname = usePathname();
  const { getResourceFromPermalink, resetPermalink, setPermalink } = usePermalink();
  const { data: session } = useSession();
  const t = useTranslations();

  const [resource, setResource] = useAtom(resourceAtom);
  const resetResource = useResetAtom(resourceAtom);

  const [open, setOpen] = useState(false);
  const [providersAuth, setProvidersAuth] = useState<ProviderAuth[] | undefined>([]);

  const setProviders = async () => {
    if (!session?.user.linkedAccounts) {
      setOpen(true);
      return;
    }

    const validStorageAccounts: ProviderAuth[] = [];

    for (const linkedAccounts of session.user.linkedAccounts) {
      if (linkedAccounts.identityProvider === 'orcid') continue;

      const isExpired = await isIDPTokenExpired(linkedAccounts.identityProvider);
      if (isExpired) continue;

      validStorageAccounts.push({
        name: linkedAccounts.identityProvider,
        access_token: linkedAccounts.access_token!,
      });
    }

    setProvidersAuth(validStorageAccounts);
    setOpen(true);
  };

  useEffect(() => {
    const permalink = getResourceFromPermalink();
    // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
    if (permalink?.valid && (permalink.resource.repo || permalink.resource.url)) {
      setResource(permalink.resource);
    }
    setProviders();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [source]);

  const validDToC = (content: string) => {
    try {
      const jsonContent = JSON.parse(content);
      const result = DToCConfigSchema.safeParse(jsonContent);
      return result.success ? { valid: true } : { valid: false, error: result.error.toString() };
    } catch (e) {
      return { valid: false, error: 'Bad DToC config :(' };
    }
  };

  const handleCancel = () => {
    setOpen(false);
    onCancel?.();
  };

  const handleOnChange = (res?: Resource) => {
    if (pathname !== '/') return;

    if (!res || res.storageSource === 'paste' || res.storageSource === 'local') {
      resetPermalink();
      resetResource();
      return;
    }

    setResource(res);
    setPermalink(res);
  };

  const handleLoad = (res: Resource) => {
    setOpen(false);
    onClose?.(res);
  };

  const handleSave = (res: Resource) => {
    setOpen(false);
    onClose?.(res);
  };

  return (
    <>
      {open && (
        <StorageDialog
          config={{
            allowedMimeTypes: ['application/json'],
            providers: providersAuth,
            preferProvider: session?.user.preferredIdentityProvider,
            validate: validDToC,
          }}
          headerLabel={label}
          onBackdropClick={type === 'load' ? handleCancel : undefined}
          onCancel={handleCancel}
          onChange={handleOnChange}
          onLoad={handleLoad}
          onSave={handleSave}
          open={open}
          resource={resource}
          source={source}
          type={type}
        />
      )}
    </>
  );
};
