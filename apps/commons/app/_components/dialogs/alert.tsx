import {
  ColorPaletteProp,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  ModalDialog,
} from '@mui/joy';
import { type PropsWithChildren } from 'react';

export interface AlertDialogProps extends PropsWithChildren {
  actions?: React.ReactNode;
  color?: ColorPaletteProp;
  header: string;
  headerIcon?: React.ReactNode;
}

export const AlertDialog = ({ actions, color, children, header, headerIcon }: AlertDialogProps) => {
  return (
    <ModalDialog role="alertdialog" variant="plain" size="sm" color={color}>
      <DialogTitle sx={{ alignItems: 'center' }}>
        {headerIcon}
        {header}
      </DialogTitle>
      <Divider />
      <DialogContent>{children}</DialogContent>
      {actions && <DialogActions>{actions}</DialogActions>}
    </ModalDialog>
  );
};
