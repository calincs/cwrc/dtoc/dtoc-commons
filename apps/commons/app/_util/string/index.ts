export const isValidHttpURL = (value: string) => {
  const res = value.match(/^http(s)?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\/\S*)?$/);
  return res !== null;
};

export const splitPathFilename = (path: string): [string, string] => {
  const pathArray = path.split('/');
  const filename = pathArray.pop() ?? '';
  path = pathArray.join('/');
  return [path, filename];
};
