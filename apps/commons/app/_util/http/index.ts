/**
 * The SwrFetcher function is a TypeScript function that fetches data from a given URL and returns it as JSON.
 * @param {RequestInfo} input - The `input` parameter is the URL or the Request object that specifies
 * the resource you want to fetch. It can be a string representing the URL or a Request object that
 * contains additional information such as headers, method, body, etc.
 * @param {RequestInit} [init] - The `init` parameter is an optional parameter of type `RequestInit`
 * that allows you to customize the request options such as headers, method, body, etc. It is used to
 * provide additional information for the fetch request.
 * @returns The function `SwrFetcher` returns a promise that resolves to a JSON object.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const SwrFetcher = async <JSON = any>(
  input: RequestInfo,
  init?: RequestInit,
): Promise<JSON> => {
  const res = await fetch(input, init);
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return res.json();
};
