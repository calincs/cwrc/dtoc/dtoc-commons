import { SwrFetcher } from '@/app/_util';
import useSWR from 'swr';
// import { IdentityProvider } from '../api/lincs/types';

import { contract } from '@lincs.project/auth-api-contract';
import { ClientInferResponseBody } from '@ts-rest/core';

type IdentityProviders = ClientInferResponseBody<typeof contract.v1.providers.getAll, 200>;
type Error500 = ClientInferResponseBody<typeof contract.v1.providers.getAll, 500>;

/**
 * The `useLoadIdentityProviders` function is a custom hook that fetches a list of identity providers
 * and sorts them alphabetically by their provider ID.
 * @returns The `useLoadIdentityProviders` function returns an object with the following properties:
 */

export const useLoadIdentityProviders = () => {
  const { data, error, isLoading } = useSWR<IdentityProviders, Error500>(
    '/api/lincs/identity-providers',
    SwrFetcher,
  );

  data?.sort((a, b) => {
    const idA = a.providerId?.toUpperCase() ?? '';
    const idB = b.providerId?.toUpperCase() ?? '';
    if (idA < idB) return -1;
    if (idA > idB) return 1;
    return 0;
  });

  return {
    providers: data,
    isLoading,
    isError: error,
  };
};
