'use client';

import { resourceAtom } from '@/jotai/store';
import type { Error } from '@/types';
import type { Resource } from '@cwrc/leafwriter-storage-service';
import { useResetAtom } from 'jotai/utils';
import Cookies from 'js-cookie';
import { signIn, useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import { useRouter, useSearchParams } from 'next/navigation';

type PermalinkResponse = {} & (Permalink | PermalinkError);

interface Permalink {
  raw: string;
  resource: Resource;
  valid: true;
}

interface PermalinkError extends Error {
  valid: false;
}

export const usePermalink = () => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const { data: session, status: sessionStatus } = useSession();
  const t = useTranslations();

  const resetResource = useResetAtom(resourceAtom);

  const getResourceFromPermalink = () => {
    if (sessionStatus === 'loading') {
      console.log('SESSION IS LOADING');
      return;
    }

    let permalinkResponse = parsePermalink();
    if (!permalinkResponse) return;
    if (!permalinkResponse.valid) return permalinkResponse;

    // //* UNAUTHENTICATED
    if (sessionStatus === 'unauthenticated') {
      Cookies.set('resource', permalinkResponse.raw, { expires: 5 / 1440 }); // 5 minutes
      signIn('keycloak', undefined, { prompt: 'login' });

      const error: PermalinkError = {
        valid: false,
        type: 'warning',
        message: 'You must sign in to access this resource',
      };
      return error;
    }

    // //* AUTHENTICATED
    const params = new URLSearchParams(Cookies.get('resource'));
    if (params.size > 0) {
      Cookies.remove('resource');
      permalinkResponse = parsePermalink(params);
      if (!permalinkResponse) return;
      if (!permalinkResponse.valid) return permalinkResponse;

      setPermalink(permalinkResponse.resource);
    }

    return permalinkResponse;
  };

  const isStorageProviderLinked = (provider: string) => {
    return session?.user.linkedAccounts?.some(
      ({ identityProvider }) => identityProvider === provider,
    );
  };

  const parsePermalink = (params?: URLSearchParams): PermalinkResponse | undefined => {
    if (!params) params = searchParams;
    if (params.size === 0) return;

    //* if it refers to URL
    const url = params.get('url');
    if (url) {
      const resource: Resource = { url, storageSource: 'url' };
      return { valid: true, raw: params.toString(), resource };
    }

    //* if it refers to a repo
    const provider = params.get('provider');
    if (!provider || !isStorageProviderLinked(provider)) {
      let message = `${t('FilePanel.warning.storage provider invalid', { provider })}.`;
      message += ` ${t('FilePanel.warning.check URL structure')}.`;
      return { valid: false, type: 'error', message };
    }

    const owner = params.get('owner') ?? undefined;
    const ownerType = params.get('ownerType') ?? undefined;
    const repo = params.get('repo') ?? undefined;
    const path = params.get('path') ?? undefined;
    const filename = params.get('filename') ?? undefined;

    return {
      valid: true,
      raw: params.toString(),
      resource: { provider, owner, ownerType, repo, path, filename, storageSource: 'cloud' },
    };
  };

  const resetPermalink = () => {
    resetResource();
    router.push('/');
  };

  const setPermalink = (value: Resource | string) => {
    let query: string;

    if (typeof value == 'string') {
      query = value;
    } else {
      const params = stringifyQuery(value);
      if (params.size === 0) return;
      query = `?${params}`;
    }

    if (query === '/') return resetPermalink();
    router.push(query);
  };

  const stringifyQuery = (query: Resource) => {
    const validParams: Record<string, string> = {};
    if (query.provider) validParams.provider = query.provider;
    if (query.owner) validParams.owner = query.owner;
    if (query.ownerType) validParams.ownerType = query.ownerType;
    if (query.repo) validParams.repo = query.repo;
    if (query.path) validParams.path = query.path;
    if (query.filename) validParams.filename = query.filename;
    if (query.url) validParams.url = query.url;

    const params = new URLSearchParams(validParams);
    return params;
  };

  return { getResourceFromPermalink, resetPermalink, setPermalink };
};
