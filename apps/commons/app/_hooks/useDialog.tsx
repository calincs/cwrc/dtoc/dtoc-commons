import { alertDialogAtom } from '@/jotai/store';
import { Button } from '@mui/joy';
import { useSetAtom } from 'jotai';
import { useTranslations } from 'next-intl';
import { PiWarning } from 'react-icons/pi';

export const useDialog = () => {
  const t = useTranslations();
  const setAlertDialog = useSetAtom(alertDialogAtom);

  const showAnnotationAlert = ({
    content,
    header,
  }: {
    content: React.ReactNode;
    header: string;
  }) => {
    setAlertDialog({
      actions: (
        <>
          <Button color="primary" onClick={() => setAlertDialog(undefined)} variant="soft">
            Create New
          </Button>
          <Button color="neutral" onClick={() => setAlertDialog(undefined)} variant="soft">
            Select Existing One
          </Button>
          <Button
            color="neutral"
            onClick={() => setAlertDialog(undefined)}
            sx={{ textTransform: 'capitalize' }}
            variant="plain"
          >
            {t('common.cancel')}
          </Button>
        </>
      ),
      children: content,
      color: 'warning',
      header,
      headerIcon: <PiWarning />,
    });
  };

  return {
    showAnnotationAlert,
  };
};
