'use client';

import { db, CommonsRecent } from '@/db';
import type { Resource } from '@cwrc/leafwriter-storage-service';
import { useTranslations } from 'next-intl';
import { enqueueSnackbar } from 'notistack';
import { v4 as uuidv4 } from 'uuid';
import { DToCConfigSchema } from '@/types/dtocDocument';


export const useFile = () => {
  const t = useTranslations();

  const parseResource = async (resource: Resource) => {
    if (resource.url) {
      return resource.url;
    }

    if (!resource.content) {
      enqueueSnackbar(t('error.failed to load document'), { variant: 'error' });
      return;
    }

    const { writePermission, storageSource, hash, ...repo } = resource;

    let content: object;
    try {
      content = JSON.parse(resource.content);
    } catch {
      enqueueSnackbar(t('error.failed to load document'), { variant: 'error' });
      return;
    }

    const result = DToCConfigSchema.safeParse(content);
    if (result.success === false) {
      enqueueSnackbar(t('error.failed to load document'), { variant: 'error' });
      return;
    }

    const id = uuidv4();
    const document: CommonsRecent = {
      id,
      lastOpenedAt: new Date(),
      source: {
        ...resource,
        content: JSON.stringify(result.data),
      },
    };

    const localId = await db.recentDocuments.add(document);
    return localId;

  };

  return {
    parseResource,
  };
};
