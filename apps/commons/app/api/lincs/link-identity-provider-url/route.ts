import { getServerSession } from 'next-auth';
import { NextRequest, NextResponse } from 'next/server';
import queryString from 'query-string';
import { authOptions } from '../_auth';
import { lincsAuthApi } from '../_auth/auth-api';

//* Needs to be dynamic becasue when building with docker the env vars are only available at run time.
export const dynamic = 'force-dynamic';

export async function GET(req: NextRequest) {
  const { searchParams } = new URL(req.url);
  const isRefresh = searchParams.get('isRefresh');
  const codeVerifier = searchParams.get('codeVerifier');
  const provider = searchParams.get('provider');

  if (!provider) return NextResponse.json({ error: 'Provider missing' });

  const session = await getServerSession(authOptions);
  if (!session?.access_token || !session?.user) {
    return NextResponse.json({ error: 'Session expired' });
  }

  const { access_token, user } = session;
  const username = user.username;
  if (!username) return NextResponse.json({ error: 'username missing' });

  const redirectUri = queryString.stringifyUrl(
    {
      url: `${process.env.NEXTAUTH_URL}/link-accounts`,
      query: { provider, isRefresh, codeVerifier },
    },
    { skipNull: true },
  );

  const response = await lincsAuthApi.users.getLinkAccountUrl({
    headers: { authorization: `Bearer ${access_token}` },
    params: { username },
    query: { provider, redirectUri },
  });

  if (response.status === 401 || response.status === 404 || response.status === 500) {
    console.warn(response.body.message);
    return NextResponse.json({
      status: response.status,
      error: response.body.message,
    });
  }

  if (response.status !== 200) {
    console.warn({ error: 'something went wrong' });
    return NextResponse.json({
      status: response.status,
      error: 'something went wrong',
    });
  }

  return NextResponse.json(response.body);
}
