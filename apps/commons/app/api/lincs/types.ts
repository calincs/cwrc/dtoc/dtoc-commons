import { contract } from '@lincs.project/auth-api-contract';
import { type ClientInferResponseBody } from '@ts-rest/core';

export type LinkedAccounts = ClientInferResponseBody<
  typeof contract.v1.users.getLinkedAccounts,
  200
>;
export type LinkedAccountInstance = LinkedAccounts[0];

export type IdentityProviders = ClientInferResponseBody<typeof contract.v1.providers.getAll, 200>;
export type IdentityProvider = IdentityProviders[0];

export interface AccountManagement {
  url: string;
  parts: {
    baseUrl: string;
    referrer?: string;
    referrer_uri?: string;
  };
}

export interface IdentityProviderToken {
  access_token: string;
  accessTokenExpiration?: number;
  scope?: string;
  token_type: string;
  expires_in?: number;
  refresh_token?: string;
  created_at?: number;
}

export interface LinkedAccount extends LinkedAccountInstance {
  access_token?: string;
  accessTokenExpires?: number;
  identityProvider: string;
  userId?: string;
  userName?: string;
  token?: IdentityProviderToken;
}
