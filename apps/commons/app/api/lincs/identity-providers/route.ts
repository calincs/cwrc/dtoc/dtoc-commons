import { NextResponse } from 'next/server';
import { lincsAuthApi } from '../_auth/auth-api';

//* Needs to be dynamic becasue when building with docker the env vars are only available at run time.
export const dynamic = 'force-dynamic';

export async function GET() {
  const response = await lincsAuthApi.providers.getAll();

  if (response.status === 200) {
    return NextResponse.json(response.body);
  }

  if (response.status === 500) {
    return NextResponse.json({ error: response.body.message });
  }

  return NextResponse.json({ error: 'something went wrong' });
}
