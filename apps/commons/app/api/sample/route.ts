import { CommonsDocument } from '@/types';
import { promises as fs } from 'fs';
import { NextResponse } from 'next/server';

//* Needs to be dynamic becasue when building with docker the env vars are only available at run time.
export const dynamic = 'force-dynamic';

export async function GET() {

  const file = await fs.readFile('public/content/sample/sample-collection.json', 'utf8');

  try {
    const collection: CommonsDocument[] = JSON.parse(file);
    return NextResponse.json<CommonsDocument[]>(collection);
  } catch {
    return NextResponse.json([]);
  }
}
