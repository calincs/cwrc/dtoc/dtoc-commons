module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: ['README.md', '.eslintrc.js', 'next.config.mjs'],
};
