export interface Message {
  success: boolean;
  error?: string;
  codeVerifier?: string;
  [x: string]: unknown;
}

export interface LinkAccountMessage extends Message {
  isRefresh?: boolean;
  provider?: string;
}
