import { z } from 'zod';

export const DToCCurationSchema = z.object({
  markup: z.array(z.object({
    xpath: z.string(),
    label: z.string().optional(),
    usage: z.string().optional()
  })).optional(),
  toc: z.array(z.string()).optional()
})

export const DToCConfigSchema = z.object({
  inputs: z.array(z.string().url()),
  documents: z.string(),
  ignoreNamespace: z.boolean().optional(),
  documentTitle: z.string().optional(),
  documentAuthor: z.string().optional(),
  documentImages: z.string().optional(),
  documentNotes: z.string().optional(),
  documentLinks: z.string().optional(),
  indexDocument: z.string().optional(),
  editionTitle: z.string().optional(),
  editionSubtitle: z.string().optional(),
  curation: DToCCurationSchema.optional()
})

export type DToCConfig = z.infer<typeof DToCConfigSchema>;
