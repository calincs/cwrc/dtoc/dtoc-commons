import { z } from 'zod';

export const RepositorySchema = z.object({
    provider: z.string(), //z.union([z.literal('github'), z.literal('gitlab')]),
    ownerType: z.string(), //z.union([z.literal('user'), z.literal('org')]),
    owner: z.string(),
    repo: z.string(),
    branch: z.string(),
    path: z.string().optional(),
    filename: z.string(),
  });
  
  export type Repository = z.infer<typeof RepositorySchema>;
  
  const CommonsSourcePasteSchema = z.object({ content: z.string() });
  const CommonsSourceDeviceSchema = CommonsSourcePasteSchema.merge(
    z.object({ filename: z.string() }),
  );
  
  const CommonsSourceRemoteSchema = z.object({
    content: z.string().optional(),
    filename: z.string().optional(),
    url: z.string().url(),
  });
  
  const CommonsSourceCloudSchema = CommonsSourceRemoteSchema.merge(
    z.object({ filename: z.string(), repository: RepositorySchema }),
  );
  
  const CommonsSourceSchema = z.union([
    CommonsSourcePasteSchema,
    CommonsSourceDeviceSchema,
    CommonsSourceRemoteSchema,
    CommonsSourceCloudSchema,
  ]);
  
  export type CommonsSource = z.infer<typeof CommonsSourceSchema>;
  
  export const CommonsDocumentSchema = z.object({
    id: z.union([z.string().url(), z.string().uuid()]).optional(),
    lastOpenedAt: z.date().optional(),
    source: CommonsSourceSchema,
  });
  
  export type CommonsDocument = z.infer<typeof CommonsDocumentSchema>;
  