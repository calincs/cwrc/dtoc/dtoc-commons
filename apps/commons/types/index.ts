export * as SiteBroadcastChannel from './broadcastChannel';
export * from './annotationCollection';
export * from './commons';
export * from './dtocDocument';

export const ErrorTypes = ['info', 'warning', 'error'] as const;
export type ErrorType = (typeof ErrorTypes)[number];

export interface Error {
  message: string;
  title?: string;
  type: ErrorType;
}
