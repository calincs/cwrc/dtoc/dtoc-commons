// Use type safe message keys with `next-intl`
type Messages = typeof import('./locales/en-CA.json');
type IntlMessages = Messages;
