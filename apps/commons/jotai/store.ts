import { AlertDialogProps } from '@/app/_components/dialogs/alert';
import { AnnotationCollection, CommonsDocument } from '@/types';
import { Resource } from '@cwrc/leafwriter-storage-service';
import { atom } from 'jotai';
import { atomWithReset } from 'jotai/utils';

export const alertDialogAtom = atom<AlertDialogProps | undefined>(undefined);
alertDialogAtom.debugLabel = 'alertDialogAtom';

export const annotationCollectionAtom = atom<AnnotationCollection | undefined>(undefined);
annotationCollectionAtom.debugLabel = 'annotationCollectionAtom';

export const documentAtom = atom<string | undefined>(undefined);
documentAtom.debugLabel = 'documentAtom';

export const resourceAtom = atomWithReset<Resource | undefined>(undefined);
resourceAtom.debugLabel = 'resourceAtom';

export const commonsDocumentAtom = atom<CommonsDocument | undefined>(undefined);
commonsDocumentAtom.debugLabel = 'commonDocumentAtom';

export const dtocAppAtom = atom<any | undefined>(undefined);
dtocAppAtom.debugLabel = 'dtocAppAtom';

export const baseUrlAtom = atom('');
baseUrlAtom.debugLabel = 'baseUrlAtom';