# changesets

Before release check workflow on changesets

[https://github.com/changesets/changesets](https://github.com/changesets/changesets)

[https://github.com/changesets/changesets/blob/main/docs/intro-to-using-changesets.md](https://github.com/changesets/changesets/blob/main/docs/intro-to-using-changesets.md)

[https://github.com/changesets/changesets/blob/main/docs/adding-a-changeset.md](https://github.com/changesets/changesets/blob/main/docs/adding-a-changeset.md)

[https://github.com/changesets/changesets/blob/main/docs/detailed-explanation.md](https://github.com/changesets/changesets/blob/main/docs/detailed-explanation.md)

[https://github.com/changesets/changesets/blob/main/docs/command-line-options.md](https://github.com/changesets/changesets/blob/main/docs/command-line-options.md)

[https://github.com/changesets/changesets/blob/main/docs/config-file-options.md](https://github.com/changesets/changesets/blob/main/docs/config-file-options.md)

### Alternative (not for monorepo)

[https://github.com/release-it/release-it](https://github.com/release-it/release-it)
