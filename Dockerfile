FROM node:20.8-alpine AS base
WORKDIR /app


# 1. Build the source code only when needed
FROM base AS builder

RUN apk add --no-cache libc6-compat
COPY . .
RUN \
  if [ -f package-lock.json ]; then npm ci; \
  else npm install; \
  fi
ENV NEXT_TELEMETRY_DISABLED=1
ENV NODE_ENV=production
RUN npm run build
RUN npm prune --production


# 2. Production image, copy only runtime files and run node
FROM base AS runner

COPY --from=builder --chown=node:node /app/apps/commons/.next/standalone/node_modules ./node_modules
COPY --from=builder --chown=node:node /app/apps/commons/.next/standalone/apps/commons .
COPY --from=builder --chown=node:node /app/apps/commons/next.config.mjs .
COPY --from=builder --chown=node:node /app/apps/commons/.next/static ./.next/static
COPY --from=builder --chown=node:node /app/apps/commons/public ./public

USER node
EXPOSE 3000
ENV PORT 3000
ENV HOSTNAME=0.0.0.0
ENV NEXT_TELEMETRY_DISABLED=1
ENV NODE_ENV=production

CMD ["node", "server.js"]